﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class CameraFollowing : MonoBehaviour        // Also ScreenShake
    {
        Camera cam;

        [SerializeField]
        Transform follow;

        Vector3 zoomPosition;

        [SerializeField, Range(0, 0.2f)]
        float smoothness = 0.1f;
        float originalZoom;

        bool isZooming;


        void Start()
        {          
            cam = Camera.main;
            originalZoom = cam.orthographicSize;
            if (follow == null) follow = GameObject.Find("Player").transform;
            transform.parent = null;                            //While it is easier to save this under the gameManger prefab, it cannot have a Parent to work properly.
            DontDestroyOnLoad(gameObject);
        }


        private void FixedUpdate()                  //in fixedupdate to remove pseudo-jitter
        {
            if (!isZooming) transform.position = Vector3.Lerp(new Vector3(Mathf.Lerp(transform.position.x, follow.position.x, smoothness), Mathf.Lerp(transform.position.y, follow.position.y, smoothness), -10), Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.01f);       //Interpolates between a smoothed out(through lerp) transform and the mouse position. Lerpception.
            else transform.position = Vector3.Lerp(new Vector3(transform.position.y, transform.position.x, -10), new Vector3(zoomPosition.x, zoomPosition.y, -10), 0.9f);
        }

        private void Update()
        {
            if (isZooming)
            {
                cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalZoom - 1, 0.3f);
            }
            else
            {
                if (Mathf.Abs(originalZoom - cam.orthographicSize) >= 0.1f) cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalZoom, 0.7f);
                else if (cam.orthographicSize != originalZoom) cam.orthographicSize = originalZoom;
            }
        }

        public IEnumerator Shake(float duration, float magnitude)                   //Screenshake, also written by tutorial
        {

            Vector3 originalPos = transform.localPosition;

            float elapsed = 0.0f;

            while (elapsed < duration)
            {
                float x = Random.Range(-3f, 3f) * magnitude;

                float y = Random.Range(-3f, 3) * magnitude;


                transform.localPosition = follow.position;

                transform.localPosition += new Vector3(x, y, originalPos.z);

                elapsed += Time.deltaTime;

                yield return null;

            }


        }

        public void starZooming(Vector3 toZoomAt)
        {
            originalZoom = cam.orthographicSize;
            zoomPosition = toZoomAt;
            StartCoroutine(zoomChange());
        }

        IEnumerator zoomChange()
        {                              
             isZooming = true;

            yield return new WaitForSeconds(0.3f);

            isZooming = false;
        }
    }
}

