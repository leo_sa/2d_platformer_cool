﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class HealthBox : MonoBehaviour
    {
        PlayerHealth health;

        AudioSource audioSource;

        void Start()
        {
            health = FindObjectOfType<PlayerHealth>();
            audioSource = GetComponent<AudioSource>();
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(-3, 3), ForceMode2D.Impulse);                         //Random Force in random Rotation
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                if (health.health < health.maxHealth)           //when player is damaged
                {
                    health.health += 25;            //no need to calculate with maxHealth, because the bar has a maxValue of maxHealth
                    health.UpdateBar();
                }
                else
                {
                    FindObjectOfType<PlayerMovement>().BuffStats();
                }

                audioSource.pitch = Random.Range(0.9f, 1.1f);
                audioSource.Play();
                GetComponent<SpriteRenderer>().enabled = false;             //destroy with delay so audio can play correctly. Spriterenderer will be disabled, so it seems as the gameobject has been destroyed.
                GetComponent<Collider2D>().enabled = false;         //disabling collider so player can inteact once.
                Destroy(gameObject, 4);
            }
        }
    }
}

