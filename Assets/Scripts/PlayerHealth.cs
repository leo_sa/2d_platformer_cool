﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class PlayerHealth : MonoBehaviour
    {
        [HideInInspector]
        public int health;

        bool dead;          //so the player can only die ONCE

        [HideInInspector]   
        public bool canGetHurt = true;      //referenced in EdgeMode

        Animator deathscreenAnimation;

        public int maxHealth = 100;

        AudioSource[] audiosources;         //all audiosources, so they can get muted for a proper death.

        GameObject[] allEnemies;          //destroying all Enemies, so the player can die Peacfully without additional Audio

        GameManager gameManager;
        Slider healthSlider;

        public GameObject HurtEffect;   //referenced in characterEditor and Edgemode

        [SerializeField]
        GameObject deathEffect, tombStone;         //additional Deatheffect

        SpriteRenderer spriteRenderer;              //used for turning the sprite color red

        Color originalColor;                    //cacheing the originalColor of the sprite renderer, to leave modularity

        CameraFollowing shake;

        Gun gun;                            //If Dead, player cannot shoot nor move. Therefore those two references have to be created, if it is supposed to work via the public bool "candostuff" or similar name.
        PlayerMovement move;
        ItemContainer[] container;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            gameManager = FindObjectOfType<GameManager>();
            healthSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();
            health = maxHealth;
            originalColor = spriteRenderer.color;
            UpdateBar();
            shake = FindObjectOfType<CameraFollowing>();
            container = GameObject.Find("Inventory").transform.GetComponentsInChildren<ItemContainer>();

            gun = FindObjectOfType<Gun>();
            move = FindObjectOfType<PlayerMovement>();

            deathscreenAnimation = GameObject.Find("Deathscreen").GetComponent<Animator>();
        }

        public void Damage(int damage)
        {
            StartCoroutine(DamageColor());
            StartCoroutine(shake.Shake(0.3f, 0.02f));
            Instantiate(HurtEffect, transform.position, Quaternion.identity);
            if(canGetHurt) health -= damage;
            gameManager.SlowMotion(0.2f, 0.03f);
            UpdateBar();
        }

        public void UpdateBar()
        {
            healthSlider.maxValue = maxHealth;
            healthSlider.value = health;

            if (health <= 0)
            {
                if (!dead) Death();               
            }
            else if (health > maxHealth) health = maxHealth;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Enemy")
            {
                if (collision.gameObject.GetComponent<damagePlayer>() && health > 0)
                {
                    Damage(collision.gameObject.GetComponent<damagePlayer>().damage);
                }
            }          
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Enemy")
            {
                if (collision.gameObject.GetComponent<damagePlayer>() && health >= 0)
                {
                    Damage(collision.gameObject.GetComponent<damagePlayer>().damage);
                }
            }
        }

        IEnumerator DamageColor()                                                   //using a coroutine to "calculate" the length of damagecolor stay
        {
            spriteRenderer.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            spriteRenderer.color = originalColor;
        }

        void Death()
        {
            health = 0;

            for (int i = 0; i < container.Length; i++)
            {                
                    if (container[i].holdsItem && health == 0)
                    {
                    if (container[i].heldItem.canHeal)
                    {
                        container[i].useItem(true);
                        health = 1;         //always one for balancing reasons
                    }
                   
                    }
                
            }

            if(health == 0)         //if no healable Item in inventory, perish
            {
                gameManager.SlowMotion(0.05f, 0.1f);
                StartCoroutine(shake.Shake(0.3f, 0.02f));

                dead = true;
                gun.canbeUsed = false;
                move.canMove = false;
                gun.GetComponent<SpriteRenderer>().enabled = false;             //making the gun invisible.
                audiosources = FindObjectsOfType<AudioSource>();
                for (int i = 0; i < audiosources.Length; i++)
                {
                    audiosources[i].enabled = false;
                }

                GameObject newTomb = Instantiate(tombStone, transform.position, transform.rotation);                    //Instantiating tombstone
                newTomb.transform.parent = transform;


                FindObjectOfType<Highscore>().SetScore();                //Set the highscore!
                spriteRenderer.enabled = false;
                Instantiate(deathEffect, transform.position, Quaternion.identity);
                StaticValues.resetValues();
                StaticValues.isDead = true;
                Invoke("DeathScreenEnable", 0.5f);
            } 
           
        }

        void DeathScreenEnable()
        {
            Invoke("DestroyAllenemies", 1);     //Invoking with delay, so the player cant see the "initial destruction"         
            deathscreenAnimation.SetTrigger("fadeIn");
        }

        void DestroyAllenemies()
        {
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < allEnemies.Length; i++)
            {
                Destroy(allEnemies[i]);
            }
        }
    }

}
