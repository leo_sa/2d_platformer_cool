﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class GameManager : MonoBehaviour                    //This here is a singleton. DungeonManager manages every Dungeon. Not a singleton.
    {
        public int level;

        public int ammoAvailable = 100;             //Starting Ammo. Has to be stored in here, because GUN will change.

        public int tier = 1;

        public int money;                   //currency/coins/money available

        int originalCutoff;             //the original Cutoff-frequency


        TextMeshProUGUI text, coinText;

        [HideInInspector]
        public TextMeshProUGUI ammoAvailableText;           //public for references in GUN;


        public bool canSetTime = true;                        //referenced in NPC
        public  bool paused;                    //also used to detect if time can be changed, but private, so enemy slow motion does not interfere with the pausescreen
        [HideInInspector]
        public bool canPause = true;            //referenced in NPC trade

        [HideInInspector]
        public float slowMotionTime;

        float coinTimer;            //delay, when the Pitch will be back to normal


        AudioSource audioSource;            //for picking up coins    


        GameObject pauseMenu;

        PlayerMovement movement;


        [HideInInspector]
        public string playerName;           //referenced in PlayerChangeName


        Animator inventoryAnim;         //when paused, inventory can be seen.


        Gun gun;            //reference, so player cannot shoot while paused

        AudioLowPassFilter lowFi;               //lowpassfilter cutoff frequency will be lower when paused for extra nice feel.

        void Start()
        {
            DontDestroyOnLoad(gameObject);
            text = GameObject.Find("Level").GetComponent<TextMeshProUGUI>();
            ammoAvailableText = GameObject.Find("AvailableAmmo").GetComponent<TextMeshProUGUI>();

            ammoAvailableText.text = "Left:" + ammoAvailable;

            coinText = GameObject.Find("Coin_Text").GetComponent<TextMeshProUGUI>();

            coinText.text = "Coins:" + money;

            audioSource = GetComponent<AudioSource>();

            pauseMenu = GameObject.Find("PauseMenu");
            pauseMenu.SetActive(false);

            if (playerName == null) playerName = "CoolPlayer";              //assign generic Name if name not defined
            else playerName = PlayerPrefs.GetString("name");

            inventoryAnim = GameObject.Find("Inventory").GetComponent<Animator>();

            gun = FindObjectOfType<Gun>();

            lowFi = FindObjectOfType<AudioLowPassFilter>();         //only one instance is set, so findobjectoftype can be used.

            originalCutoff = (int)lowFi.cutoffFrequency;

            AudioListener.volume = PlayerPrefs.GetFloat("AudioLis", 1);

            movement = FindObjectOfType<PlayerMovement>();
        }


        void Update()
        {
            if (ammoAvailable < 0) ammoAvailable = 0;
            text.text = "Level: " + level;
            if (Input.GetKeyDown(KeyCode.Escape) && canPause)               //activating pause
            {
                if (pauseMenu.activeInHierarchy)
                {
                    paused = false;
                    Time.timeScale = 1;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;       //setting fixeddeltatime back to normal
                    inventoryAnim.SetTrigger("fadeOut");
                    inventoryAnim.ResetTrigger("fadeIn");           //fadeIn should not be set when its fading out
                    pauseMenu.SetActive(false);
                    lowFi.cutoffFrequency = originalCutoff;
                }
                else
                {
                    paused = true;
                    Time.timeScale = 0.01f;         //not zero, so animations can still play (inventory)
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;       //setting fixeddeltatime, so the background runs smooth
                    inventoryAnim.SetTrigger("fadeIn");
                    inventoryAnim.ResetTrigger("fadeOut");           //fadeOut should not be set when its fading in
                    pauseMenu.SetActive(true);
                    lowFi.cutoffFrequency = 400;
                }
                gun.canbeUsed = !paused;        //can the gun shoot?
            }

            if (canSetTime)
            {
                if (!paused)
                {
                    if(Time.timeScale != 1) TimeCheck();                    //time cannot be set to normal, when the game is paused.
                }
               
            }
            else SlowMotionLength();

            if (coinTimer > 0) coinTimer -= Time.deltaTime;
            else if (coinTimer < 0)
            {
                audioSource.pitch = Random.Range(0.95f, 1.05f);
                coinTimer = 0;
            }
        }

        void TimeCheck()
        {
            while (Time.timeScale <= 1f)
            {
                Time.timeScale += Time.deltaTime;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;                                                           //Time.fixedDeltaTime is 0.02f. But for better understandin inside the game-Manager script ill use "=" instead of *= and also hardcoding the values.
            }

            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;                       //theoretically, the multiplication is useless, but it helps understandin the code.

        }


        void SlowMotionLength()
        {
            if(slowMotionTime > 0)
            {
                slowMotionTime -= Time.deltaTime;
            }
            else if(slowMotionTime < 0)
            {
                canSetTime = true;
                slowMotionTime = 0;
            }
        }

        public void SlowMotion(float timescale, float length)
        {
            slowMotionTime = length;

            canSetTime = false;

           
                Time.timeScale = timescale;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;                                                           //Time.fixedDeltaTime is 0.02f. But for better understandin inside the game-Manager script ill use "=" instead of *= and also hardcoding the values.
            
        }

        public void RefreshCoinText()               //used as a void to avoid using it in Update. Also used to determine coinPitch and Timing
        {
            coinText.text = "Coins:" + money;
            if (audioSource.pitch <= 1.9f) audioSource.pitch += 0.03f;
            audioSource.Play();
            coinTimer = 0.5f;
        }

        public void RefreshAmmoText()
        {
            if (ammoAvailable < 0) ammoAvailable = 0;
            ammoAvailableText.text = "Left:" + ammoAvailable;
        }

        public void PauseOrEnablePlayer(bool activator)
        {
            gun.canbeUsed = activator;
            movement.canMove = activator;
        }
    }
}
