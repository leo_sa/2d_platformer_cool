﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;                //for Highscore display


namespace CoATwoPR
{
    public class Highscore : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI timerText, highScoreTimerText;

        TextMeshProUGUI text;

        int highscore;

        [SerializeField]
        string highScoreKey;

        float timeCounter;

        [SerializeField]
        bool usedInPlatformer, beginning;

        bool setOnce;


        void Start()
        {
            text = GetComponent<TextMeshProUGUI>();
        }

        public void SetScore()
        {
            highscore = FindObjectOfType<GameManager>().level;

            int temporaryActualHighscore = PlayerPrefs.GetInt("highscore");

            if (highscore > temporaryActualHighscore)
            {
                PlayerPrefs.SetInt("highscore", highscore);
                text.text = "Although you faded into the infinite vastness my dear, you made it further than ever: " + highscore + " Levels lie between you and me.";
                PlayerPrefs.Save();
            }
            else
            {
                text.text = "Heartbroken you crumble down after " + highscore + " levels. Someday you will make it past " + temporaryActualHighscore + " Levels my honey.";
            }
                
        }

        private void Update()
        {
            if (StaticValues.canCountTime && usedInPlatformer)
            {
                timeCounter += Time.deltaTime;
                timerText.text = timeCounter.ToString();
            }
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject.name == "Player")
            {
                if (!setOnce)
                {
                    if (beginning)
                    {
                        StaticValues.canCountTime = true;
                    }
                    else
                    {
                        StaticValues.canCountTime = false;

                        if (PlayerPrefs.GetFloat(highScoreKey) > 0)
                        {
                            if (timeCounter < PlayerPrefs.GetFloat(highScoreKey))           //in this case a new highscore is achieved
                            {
                                PlayerPrefs.SetFloat(highScoreKey, timeCounter);
                                highScoreTimerText.text = "You did it! New Highscore: " + PlayerPrefs.GetFloat(highScoreKey);
                            }
                            else
                            {
                                highScoreTimerText.text = "Not fast enough! Old highscore: " + PlayerPrefs.GetFloat(highScoreKey);
                            }
                        }
                        else
                        {
                            PlayerPrefs.SetFloat(highScoreKey, timeCounter);
                            highScoreTimerText.text = "You did it! New Highscore: " + "<#001aff>" + PlayerPrefs.GetFloat(highScoreKey) + "</color>";
                        }
                        

                    }

                    setOnce = true;
                }
            }        
        }

    }
}

