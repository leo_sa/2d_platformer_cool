﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class FlipSpriteXY : MonoBehaviour
    {
        [SerializeField]
        bool x, y;                          //FlipSpriteX or Y?

        Transform followObject;
        [HideInInspector]
        public SpriteRenderer spriteRenderer;           //used in PlayerMovement

        [SerializeField]
        string follow;

        [HideInInspector]
        public bool canFlip;            //referenced in PlayerMovement
        public bool followMouse;                    //Is the Mouse the "FlipSetter"?

        Camera cam;

        void Start()
        {
            canFlip = true;
            if (!followMouse) followObject = GameObject.Find(follow).transform;
            spriteRenderer = GetComponent<SpriteRenderer>();
            cam = Camera.main;
        }

        void Update()
        {
            if (canFlip)
            {
                if (!followMouse)
                {
                    if ((transform.position.x - followObject.position.x) <= 0)
                    {
                        if (x) spriteRenderer.flipX = false;
                        if (y) spriteRenderer.flipY = false;
                    }
                    else
                    {
                        if (x) spriteRenderer.flipX = true;
                        if (y) spriteRenderer.flipY = true;
                    }
                }
                else
                {
                    if ((transform.position.x - cam.ScreenToWorldPoint(Input.mousePosition).x <= 0))
                    {
                        if (x) spriteRenderer.flipX = false;
                        if (y) spriteRenderer.flipY = false;
                    }
                    else
                    {
                        if (x) spriteRenderer.flipX = true;
                        if (y) spriteRenderer.flipY = true;
                    }
                }

            }


        }
    }
}

