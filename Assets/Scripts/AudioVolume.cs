﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class AudioVolume : MonoBehaviour
    {
        Slider slider;

        private void Start()
        {
            slider = GetComponent<Slider>();
            slider.value = PlayerPrefs.GetFloat("AudioLis", 1);
        }

        public void ChangeAudioVolume()
        {
            PlayerPrefs.SetFloat("AudioLis", slider.value);
            PlayerPrefs.Save();

            AudioListener.volume = slider.value;
        }
    }

}
