﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ScriptableItem", order = 2)]
    public class scriptableItem : ScriptableObject
    {
        public string itemName;
        public float worth;
        public Sprite sprite;
        public bool canHeal;
    }

}
