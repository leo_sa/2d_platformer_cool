﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class StartRotation : MonoBehaviour
    {
        [SerializeField, Range(0, 360)]
        float accuracy;

        void Start()
        {
            transform.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + Random.Range(-accuracy / 2, accuracy / 2));
        }
    }

}
