﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class ActualRay : MonoBehaviour
    {
        LineRenderer line;

        Transform origin;

        Vector2 position;  
        
        CapsuleCollider2D capsuleCollider;
       
        void Start()
        {
            capsuleCollider = GetComponent<CapsuleCollider2D>();
            line = GetComponent<LineRenderer>();
            origin = GameObject.Find("Player").transform;
            position = transform.position;

        }


        void Update()           //Line is drawn from position to player
        {
            float distance = Vector2.Distance(transform.position, -origin.transform.position);

            capsuleCollider.size = new Vector2(distance, capsuleCollider.size.y);
            capsuleCollider.offset = new Vector2(-(distance / 2), capsuleCollider.offset.y);

            line.SetPosition(0, position);
            line.SetPosition(1, origin.transform.position);
        }
    }
}

