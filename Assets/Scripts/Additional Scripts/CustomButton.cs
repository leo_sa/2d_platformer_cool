﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class CustomButton : MonoBehaviour
    {
        [SerializeField]
        LoadSceneMode loadScene;

        [SerializeField]
        UnityEvent onClickEvent;

        [SerializeField]
        Sprite unActive, active;

        SpriteRenderer spriteRenderer;

        [SerializeField]
        string levelName;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void loadSene()
        {
            SceneManager.LoadScene(levelName, loadScene);
        }

        private void OnMouseOver()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) onClickEvent.Invoke();
            spriteRenderer.sprite = active;
        }

        private void OnMouseExit()
        {
            spriteRenderer.sprite = unActive;
        }
    }

}
