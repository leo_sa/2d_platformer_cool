﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class AudioSourceAdditions : MonoBehaviour
    {
        [SerializeField]
        bool randomPitch, playOnCollison;                       //the bools to activate for addition

        AudioSource audioSource;                
        
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            if (randomPitch) audioSource.pitch = Random.Range(0.95f, 1.05f);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (playOnCollison)
            {
                if (randomPitch) audioSource.pitch = Random.Range(0.95f, 1.05f);
                audioSource.Play();
            }
        }
    }

}
