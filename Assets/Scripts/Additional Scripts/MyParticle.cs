﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class MyParticle : MonoBehaviour
    {
        Rigidbody2D rb2d;

        [SerializeField]
        LayerMask lay;

        [SerializeField]
        bool destroyOnContact;

        [SerializeField]
        bool randomForce;

        [SerializeField]
        bool canDie = true, canDamageEnemies;

        [SerializeField]
        float forceY, forceX;                   //Vertical and Horizotnal Force

        [SerializeField]
        float destroyDelay = 10;             //When to destroy finally?

        [SerializeField]
        bool playSoundOnCollision;                      //is the sound played on collision or instantly?

        AudioSource audioSource;


        [SerializeField]
        GameObject hitEffect;

        void Start()
        {
            rb2d = GetComponent<Rigidbody2D>();
            if (randomForce)                                            //Random Force and Rotation
            {
                forceY = Random.Range(-forceY, forceY);
                forceX = Random.Range(-forceX, forceX);

                transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            }
            rb2d.AddForce(Vector2.up * forceY, ForceMode2D.Impulse);
            rb2d.AddForce(transform.right * -forceX, ForceMode2D.Impulse);
            if(canDie) Destroy(gameObject, destroyDelay);

            if (GetComponent<AudioSource>()) audioSource = GetComponent<AudioSource>();          
            
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (playSoundOnCollision)
            {
                audioSource.pitch = Random.Range(0.9f, 1.1f);
                audioSource.Play();
            }
            if (destroyOnContact) Destroy(gameObject);

            if (canDamageEnemies)       //will also damage Player
            {
                if(!Physics2D.Raycast(transform.position, Vector2.up, -0.05f, lay))
                {
                    if (collision.gameObject.GetComponent<Enemy>())
                    {
                        if (Mathf.Abs(rb2d.velocity.magnitude) > 1)
                        {
                            collision.gameObject.GetComponent<Enemy>().manualDamage((int)Mathf.Abs(rb2d.velocity.magnitude) * 2 * (int)(2 - collision.gameObject.GetComponent<Enemy>().weight), 0.01f, 0.8f);              //damaging the enemy with the speed
                            Instantiate(hitEffect, collision.GetContact(0).point, transform.rotation);
                        }
                    }
                    else if (collision.gameObject.GetComponent<PlayerHealth>())
                    {
                        if (Mathf.Abs(rb2d.velocity.magnitude) > 5) collision.gameObject.GetComponent<PlayerHealth>().Damage((int)Mathf.Abs(rb2d.velocity.magnitude));              //damaging the enemy with the speed. Also taking weight class into consideration. Light enemies will be damaged harder.
                    }
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (playSoundOnCollision)
            {
                audioSource.pitch = Random.Range(0.9f, 1.1f);
                audioSource.Play();
            }
            if (destroyOnContact) Destroy(gameObject);
        }


    }
}

