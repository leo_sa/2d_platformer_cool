﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ScriptableGun", order = 1)]
    public class ScriptableGun : ScriptableObject
    {
       
        public GameObject projectile;             //Projectile, and the Meele "Projectile" and/or effect, which damages the enemy.

        public GameObject smokeEffect;         //a Smokeffectr to add PEW for the first shot

        public GameObject barrelSmoke;             //Smoke for Barrel

        public float recoil = 1;

        public bool fullAuto;                  //is The gun fully Automatic?

        public int maxAmmo;                        //The Maximal amount of ammo

        public bool closedBolt;                //MaxAmmo +1 reload mechanic?

        public GameObject shell;

        public AudioClip shootAudio, reloadAudio, reloadTactical, hotHissing, tail, emptyClick, safeClick;              //Audioclips for the audioSource

        public float RPM = 300;

        public RuntimeAnimatorController animator;

        public Sprite worldSprite;              //A sprite for the Item-Pickup version

        public float reloadTime, tacReloadTime;

        public GameObject magazine;

        public float magazineDelay;

        public int amountOfProjectiles;

        public int additionalAmmoUsage;

        public bool usesMagazineAmmo;

        public bool useMuzzleFlash;

    }
}

