﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class MonoBehaviour2 : MonoBehaviour         //created 02.06.2020
    {
        delegate void chanceEffects();

        chanceEffects changeEffects;

        bool pauseGame;

        float originalTimeScale;


        public void Log(object tolog)
        {
            Debug.Log(tolog);
        }

        public void MyPlay(AudioSource audioSource, AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.pitch = Random.Range(0.9f, 1.1f);
            audioSource.Play();
        }

    }

}
