﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class NPC : MonoBehaviour                //"Trader" Would be a more appropriate name...
    {
        [SerializeField]
        bool isTrader;
        bool canInteract;
        bool isTrading;


        Animator animator;
        Animator inventoryAnim;
        Animator traderInventoryAnim;


        AudioSource audioSource;
        [SerializeField]
        AudioClip inAudio, outAudio;


        GameManager gManager;
        Gun gun;                //reference, so the player cant interact
        PlayerMovement playerMove;

        ItemContainer[] container;          //all containers have to be set into trade mode.
        TraderItemContainer[] traderItemContainers;

        private void Start()
        {
            if (GetComponent<Animator>()) animator = GetComponent<Animator>();
            if (GetComponent<AudioSource>()) audioSource = GetComponent<AudioSource>();

            if (isTrader)
            {
                try
                {
                    traderInventoryAnim = transform.GetChild(1).GetChild(0).GetComponent<Animator>();               
                }
                catch
                {
                    traderInventoryAnim = transform.GetChild(0).GetChild(0).GetComponent<Animator>();   //if its not the second, it is the first
                }
                container = GameObject.Find("Inventory").transform.GetComponentsInChildren<ItemContainer>();
            }


            inventoryAnim = GameObject.Find("Inventory").GetComponent<Animator>();
            gManager = FindObjectOfType<GameManager>();

            gun = FindObjectOfType<Gun>();
            playerMove = FindObjectOfType<PlayerMovement>();

            traderItemContainers = GetComponentsInChildren<TraderItemContainer>();
        }

        private void Update()
        {
            if (canInteract)
            {
                if (isTrader)
                {
                    if (Input.GetKeyDown("e")) Trade();
                }
               
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {          
            if (collision.gameObject.name == "Player")
            {              
                if (audioSource != null)
                {
                    audioSource.clip = inAudio;
                    audioSource.pitch = Random.Range(0.95f, 1.05f);
                    audioSource.Play();
                }
                canInteract = true;

                if(animator != null)
                {
                    animator.SetTrigger("activate");
                    animator.ResetTrigger("deactivate");
                }
               
            }
           
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            
            if (collision.gameObject.name == "Player")
            {
                if (audioSource != null)
                {
                    audioSource.clip = outAudio;
                    audioSource.pitch = Random.Range(0.95f, 1.05f);
                    audioSource.Play();
                }

               

                if(animator != null)
                {
                    animator.SetTrigger("deactivate");
                    animator.ResetTrigger("activate");

                    inventoryAnim.SetTrigger("fadeOut");
                    inventoryAnim.ResetTrigger("fadeIn");           //fadeIn should not be set when its fading out
                }
               

                canInteract = false;
            }

           
        }

        void Trade()                //some code copied from class GameManager
        {
            if (!isTrading)                 //Lets trade!
            {
                gManager.canSetTime = false;
                gManager.canPause = false;              //The Trade should not interfere with the inventory animations and not with the pause.
                Time.timeScale = 0.01f;         //not zero, so animations can still play (inventory)
                Time.fixedDeltaTime = 0.02f * Time.timeScale;       //setting fixeddeltatime, so the background runs smooth
                gun.canbeUsed = false;
                playerMove.canMove = false;               

                inventoryAnim.SetTrigger("fadeIn");
                inventoryAnim.ResetTrigger("fadeOut");           //fadeIn should not be set when its fading out

                traderInventoryAnim.SetTrigger("fadeIn");
                traderInventoryAnim.ResetTrigger("fadeOut");           

                isTrading = true;

                for (int i = 0; i < container.Length; i++)              //activating trade
                {
                    container[i].tradeMode = true;
                }


                for (int i = 0; i < traderItemContainers.Length; i++)             //refreshing price for every container
                {
                    traderItemContainers[i].refreshPrice();
                }
            }
            else   //Lets stop trading!
            {
                gManager.canSetTime = true;
                gManager.canPause = true;
                Time.timeScale = 1f;         
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                gun.canbeUsed = true;
                playerMove.canMove = true;

                inventoryAnim.SetTrigger("fadeOut");
                inventoryAnim.ResetTrigger("fadeIn");           //fadeIn should not be set when its fading out

                traderInventoryAnim.SetTrigger("fadeOut");
                traderInventoryAnim.ResetTrigger("fadeIn");

                for (int i = 0; i < container.Length; i++)              //deactivating trade
                {
                    container[i].tradeMode = false;
                }             
                isTrading = false;
            }
           
        }
    }
}

