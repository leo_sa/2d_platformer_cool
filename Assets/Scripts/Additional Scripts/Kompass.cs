﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;          //for onSceneLoaded

namespace CoATwoPR
{
    public class Kompass : MonoBehaviour2
    {

        float rotZ;
        [SerializeField]
        float offset;

        bool canRotate;

        Transform follow, player;

        DungeonManager dungeonManager;

        RectTransform transform_r;          //RectTransform because of UI


        private void Start()
        {
            SceneManager.sceneLoaded += setValues;          //subscribing to the delegate sceneLoaded(Scene, LoadSceneMode)
            transform_r = GetComponent<RectTransform>();
            player = GameObject.Find("Player").transform;
        }

        public void setValues(Scene scene, LoadSceneMode mode)          //will only be activated AFTER first entrance of the dungeon
        {
            if (SceneManager.GetActiveScene().name == "Dungeon")
            {
                canRotate = true;
                dungeonManager = FindObjectOfType<DungeonManager>();
                if(dungeonManager.enemies.Count != 0) follow = dungeonManager.enemies[dungeonManager.enemies.Count - 1].transform;
            }
            else canRotate = false;         //this way else will not rotate
           
        }

        private void Update()
        {
            if (canRotate)
            {
               
                    if(dungeonManager.enemies.Count > 0)            //if still enemies in the scene
                    {
                        if (follow != null) follow = dungeonManager.enemies[dungeonManager.enemies.Count - 1].transform;

                        else
                        {
                             try
                             {
                                 if (dungeonManager != null) follow = dungeonManager.enemies[dungeonManager.enemies.Count - 1].transform;               //try Catch, so no Missingreference-Error is shown in console
                             }                  
                             catch
                             {
                                 //Debug.Log("Reloaded Game");
                             }
                            
                        }
                    }
                    else
                    {
                        if (GameObject.Find("Dungeon_Exit(Clone)")) follow = GameObject.Find("Dungeon_Exit(Clone)").transform;                                                               //else DungeonExit spawned                     
                    }

              
                    if(follow != null)
                    {
                    Vector2 difference = player.transform.position - follow.position;
                    rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;                                                         //Again copied from RotateToMouse and customized
                    }
              

                transform_r.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);              
            }
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= setValues;
        }
    }

}
