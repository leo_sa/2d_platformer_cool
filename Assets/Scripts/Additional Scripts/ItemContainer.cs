﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;           //for Image references
using TMPro;                // for text set;

namespace CoATwoPR
{
    public class ItemContainer : MonoBehaviour
    {

        Sprite itemSprite;
        Sprite originalSprite;

        GameManager manager;        //for trades

        Image image;

        TextMeshProUGUI textName, textWorth, edibleText;

        PlayerHealth health;

        int worth;

        [HideInInspector]
        public bool holdsItem;
        bool canHealPlayer;

        [HideInInspector]
        public bool tradeMode;

        string Name;


        [SerializeField]
        AudioClip sellClip, eatClip;
        AudioSource audioSource;

        public scriptableItem heldItem;

        private void Start()
        {
            image = transform.GetChild(0).GetChild(1).GetComponent<Image>();
            originalSprite = image.sprite;
            textName = transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
            textWorth = transform.GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>();
            edibleText = transform.GetChild(0).GetChild(3).GetComponent<TextMeshProUGUI>();
            health = FindObjectOfType<PlayerHealth>();
            audioSource = GetComponent<AudioSource>();
            manager = FindObjectOfType<GameManager>();
        }

        public void destroyItem()
        {
            if (worth != 0)          //could be (nearly) any variable, just checks if item is held
            {
                heldItem = null;
                image.sprite = originalSprite;
                worth = 0;
                textName.text = "Name";
                textWorth.text = "worth";
            }
            holdsItem = false;
        }

        public void addItem(scriptableItem item)
        {
            Name = item.itemName;
            worth = (int)item.worth;
            itemSprite = item.sprite;
            image.sprite = itemSprite;
            textName.text = item.itemName;
            textWorth.text = worth.ToString();
            canHealPlayer = item.canHeal;
            heldItem = item;

            if (canHealPlayer) edibleText.text = "tasty";
            else edibleText.text = "disgusting";

            holdsItem = true;
        }

        public void useItem(bool saveThePlayer)
        {
            if (holdsItem)
            {
                if (tradeMode) Sell();
                else if (canHealPlayer && !saveThePlayer) HealPlayer();
                destroyItem();
            }        
        }

        void Sell()
        {
            audioSource.clip = sellClip;
            audioSource.pitch = Random.Range(0.95f, 1.05f);
            audioSource.Play();

            manager.money += worth;
            manager.RefreshCoinText();
        }


        void HealPlayer()
        {
            audioSource.clip = eatClip;
            audioSource.pitch = Random.Range(0.95f, 1.05f);
            audioSource.Play();

            health.health += worth;
            health.UpdateBar();
        }
    }


}
