﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class AmmunitionBox : MonoBehaviour
    {
        GameManager gameManager;

        int ammoToGive;

        AudioSource audioSource;

        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            ammoToGive = 20;
            audioSource = GetComponent<AudioSource>();
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(-3, 3), ForceMode2D.Impulse);                         //Random Force in random Rotation
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject.name == "Player")
            {
                gameManager.ammoAvailable += ammoToGive;
                gameManager.ammoAvailableText.text = "Left:" + gameManager.ammoAvailable;

                audioSource.pitch = Random.Range(0.9f, 1.1f);                   
                audioSource.Play();
                GetComponent<SpriteRenderer>().enabled = false;             //destroy with delay so audio can play correctly. Spriterenderer will be dissabled, so it seems as the gameobject has been destroyed.
                GetComponent<Collider2D>().enabled = false;         //disabling collider so player can inteact once.
                Destroy(gameObject, 4);
            }           
        }
    }
}

