﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class ChoosePerk : MonoBehaviour
    {
        delegate void activePerk();
        activePerk perkActive;                              //for perks

        [SerializeField]
        GameObject extraPerk;
        GameObject chooseInterface;

        Gun gun;

        TextMeshProUGUI nameText, descText;

        [SerializeField]
        Sprite[] perkSprites;                    
        Image image;

        [SerializeField]
        RuntimeAnimatorController[] perkUIcontroller;

        private void Start()
        {
            gun = FindObjectOfType<Gun>();            
            chooseInterface = transform.parent.gameObject;

            nameText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            descText = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            image = GetComponent<Image>();

            assignPerk();
        }


        void assignPerk()
        {
            int i;
            i = Random.Range(0, perkSprites.Length);         // random Perk

            switch (i)
            {
                case 0:
                    perkActive += AdditionalProjectile;
                    nameText.text = "MORE BANG";
                    descText.text = "Shoot ONE additional Projectile, but use more Ammo per shot(also ONE)";
                    image.sprite = perkSprites[0];
                    break;

                case 1:
                    if (!PerkManager.hasRecyclePerk)
                    {
                        perkActive += RecycleHit;
                        nameText.text = "No waste";
                        descText.text = "Every shot you land will recycle the shot back";
                        image.sprite = perkSprites[1];
                    }
                    else
                    {
                        perkActive += AdditionalProjectile;
                        nameText.text = "MORE BANG";
                        descText.text = "Shoot ONE additional Projectile, but use more Ammo per shot(also ONE)";
                        image.sprite = perkSprites[0];
                    }
                    break;

                case 2:
                    perkActive += AmmoPerk;
                    nameText.text = "AMMO UP";
                    descText.text = "Gain 100 Ammunition instantly";                   
                    image.sprite = perkSprites[2];
                    break;

                case 3:
                    perkActive += HealPerk;
                    nameText.text = "HEAL UP";
                    descText.text = "Get 5 additional HP and get healed to 100% instantly";
                    image.sprite = perkSprites[3];
                    break;
                    
            }           
        }


        public void choosePerk()
        {
            perkActive();
            chooseInterface.SetActive(false);
        }

        public void AdditionalProjectile()          //0
        {
            GameObject extra = Instantiate(extraPerk, transform.position, Quaternion.identity);
            extra.transform.SetParent(GameObject.Find("PerkCounter").transform, false);
            extra.GetComponent<Animator>().runtimeAnimatorController = perkUIcontroller[0];

            gun.additionalAmmoUsage++;
            gun.additionalProjectiles++;
        }

        public void RecycleHit()        //1
        {
            GameObject extra = Instantiate(extraPerk, transform.position, Quaternion.identity);
            extra.transform.SetParent(GameObject.Find("PerkCounter").transform, false);
            extra.GetComponent<Animator>().runtimeAnimatorController = perkUIcontroller[1];

            PerkManager.hasRecyclePerk = true;
        }

        public void AmmoPerk()
        {
            GameObject extra = Instantiate(extraPerk, transform.position, Quaternion.identity);
            extra.transform.SetParent(GameObject.Find("PerkCounter").transform, false);
            extra.GetComponent<Animator>().runtimeAnimatorController = perkUIcontroller[2];

            GameManager gm;
            gm = FindObjectOfType<GameManager>();
            gm.ammoAvailable += 100;
            gm.RefreshAmmoText();
        }

        public void HealPerk()
        {
            GameObject extra = Instantiate(extraPerk, transform.position, Quaternion.identity);
            extra.transform.SetParent(GameObject.Find("PerkCounter").transform, false);
            extra.GetComponent<Animator>().runtimeAnimatorController = perkUIcontroller[3];

            PlayerHealth ph;
            ph = FindObjectOfType<PlayerHealth>();
            ph.maxHealth += 5;
            ph.health = ph.maxHealth;
            ph.UpdateBar();

        }
    }

}
