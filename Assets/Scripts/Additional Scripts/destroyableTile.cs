﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{  
    public class destroyableTile : MonoBehaviour
    {
        [SerializeField]
        GameObject deathEffect;

        Transform player;

        void Start()
        {
            player = GameObject.Find("Player").transform;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<damagePlayer>()) Death();
            else if (collision.gameObject.GetComponent<Bullet>()) Death();
            else if (collision.gameObject.GetComponent<meeleeHit>()) Death();
            else if (collision.gameObject.GetComponent<destroyableTile>()) Destroy(gameObject); //when colliding with another destroyable Tile, it is probably spawned wrong.
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<damagePlayer>()) Death();
            else if (collision.GetComponent<Bullet>()) Death();
            else if (collision.GetComponent<meeleeHit>()) Death();
        }

        void Death()
        {
            if(Vector2.Distance(player.position, transform.position) <= 10)             //only die when Player is close enough
            {
                Instantiate(deathEffect, transform.position, transform.rotation);
                Destroy(gameObject);
            }           
        }
    }

}
