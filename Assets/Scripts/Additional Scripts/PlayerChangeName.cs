﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class PlayerChangeName : MonoBehaviour
    {
        [HideInInspector]
        public string playerName;

        bool canWrite;

        TextMeshProUGUI text;

        GameManager gameManager;

        Gun gun;

        PlayerMovement playermove;              //Reference to both instances, to disable "Player Input" while writing

        void Start()
        {
            text = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            if (PlayerPrefs.GetString("name", playerName) == null) text.text = "Click to begin and Save!";
            else text.text = PlayerPrefs.GetString("name", playerName);

            gun = FindObjectOfType<Gun>();
            playermove = FindObjectOfType<PlayerMovement>();

            gameManager = FindObjectOfType<GameManager>();
        }


        void Update()
        {
            if (canWrite)
            {
                text.text = playerName;
                playerName += Input.inputString;
                if (Input.GetKeyDown(KeyCode.Backspace)) playerName = null;

                if (Input.GetKeyDown(KeyCode.Return)) changeBoolean();
            }
        }

        public void changeBoolean()
        {
            if (canWrite)
            {
                gameManager.playerName = playerName;
                PlayerPrefs.SetString("name", playerName);
                PlayerPrefs.Save();

                gun.canbeUsed = true;
                playermove.canMove = true;
                canWrite = false;
            }
            else
            {
                gun.canbeUsed = false;
                playermove.canMove = false;             
                canWrite = true;
            }
        }
    }

}
