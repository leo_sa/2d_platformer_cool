﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class HostileCritter : MonoBehaviour
    {
        Rigidbody2D body2D;

        Enemy thisEnemy;

        [SerializeField]
        int damage = 3;

        [SerializeField]
        float speed;

        [SerializeField]
        bool dieOnImpact = true;

        void Start()
        {
            body2D = GetComponent<Rigidbody2D>();

            if (GetComponent<Enemy>()) thisEnemy = GetComponent<Enemy>();
        }


        void Update()
        {
            body2D.AddForce(transform.right * speed * Time.deltaTime * 80, ForceMode2D.Impulse);
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (dieOnImpact)
            {
                if (collision.gameObject.name == "Player")
                {
                    if (thisEnemy != null)
                    {
                        thisEnemy.die();
                    }
                }
            }
           
        }

    }

}
