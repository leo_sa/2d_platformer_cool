﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class EnableIfTutorialDone : MonoBehaviour
    {

        void Start()
        {
            if (PlayerPrefs.GetInt("hasDoneTutorial") == 1) gameObject.SetActive(true);         //this value is only One after the Player has finished the Tutorial
            else gameObject.SetActive(false);
        }

    }

}
