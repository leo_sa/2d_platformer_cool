﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Unicorn : MonoBehaviour
    {
        int dir = -1;                                                       //direction

        float checkspeed = 1;
        float speed;
        [SerializeField]
        float maxSpeed;


        [SerializeField]
        LayerMask lay;


        Rigidbody2D body2D;


        SpriteRenderer sprite;

        RaycastHit2D hit;

        Transform raycastPosition;

        private void Start()
        {
            maxSpeed *= Random.Range(0.95f, 1.05f);             //Adding randomness for design purposes
            body2D = GetComponent<Rigidbody2D>();
            sprite = GetComponent<SpriteRenderer>();
            raycastPosition = transform.GetChild(0).transform;
        }

        void Update()
        {
            hit = Physics2D.Raycast(raycastPosition.position, Vector2.right, dir, lay);

            if (hit.collider != null) ChangeDirection();

            speed = Mathf.Lerp(speed, maxSpeed, 0.05f);

            body2D.velocity = new Vector2(speed, body2D.velocity.y);

        }

        void ChangeDirection()
        {

            if (sprite.flipX) sprite.flipX = false;
            else sprite.flipX = true;
            dir *= -1;
            maxSpeed *= -1;                                            //Changing direction if colliding         

        }
    }

}
