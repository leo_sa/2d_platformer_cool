﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ScriptableMeeleeWeapon", order = 3)]
    public class scriptableMeeleeWeapon : ScriptableObject
    {
        public string weaponName;

        public int ammoUsage;

        public float windUpLength, hitStayLength;
        public float verticalForce, horizontalForce;
        public float accuracySpeed;
        public float unAttackDelay;             //Animation delay for continual Input

        public bool useContinualInput;          //use input.get(...)down or Input.get(...)
        public bool onClickSpawn;               //(when bool above true) spawn meeleeHit on click or when click Up?
        public bool useAnimationContinual;      //use animation when continual input?
        public bool dontDestroyOnUp;                //Dont destroy "Projectile" When RMb is up

        public GameObject meeleeHit;
        public RuntimeAnimatorController weaponAnimation;
        public Sprite worldSprite, itemSprite;
        public AudioClip useClip, spanClip;
    }

}
