﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class OldGum : MonoBehaviour2
    {
        Transform followPlayer;
        Transform bulletPosition;

        Rigidbody2D rb2d;

        GameManager manager;            //for Slowmotion
        DungeonManager dungeonManager;      //for enemy list

        [SerializeField]
        float offset;
        float rotZ;

        [SerializeField]
        int health = 400;

        Slider healthSlider;

        [SerializeField]
        GameObject bullet;

        [SerializeField]
        GameObject hitEffect, deathEffect, exit;


        private void Start()
        {
            dungeonManager = FindObjectOfType<DungeonManager>();
            dungeonManager.enemies.Add(gameObject);

            followPlayer = GameObject.Find("Player").transform;
            healthSlider = GetComponentInChildren<Slider>();

            manager = FindObjectOfType<GameManager>();

            bulletPosition = transform.GetChild(0).transform;

            rb2d = GetComponent<Rigidbody2D>();

            healthSlider.maxValue = health;
            healthSlider.value = health;
        }


        void Rotate()
        {
            Vector2 difference = followPlayer.position - transform.position;
            rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;                                                         //The angle of the Tangent in Radians, which leads to the MousePos, is converted intro degrees for EULER-Usage.

            transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);         
        }


        private void Update()
        {
            Rotate();
            rb2d.AddForce(transform.right * -3);

        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            
            if (collision.gameObject.GetComponent<Bullet>())
            {
                Instantiate(hitEffect, transform.position, transform.rotation);
                health -= collision.gameObject.GetComponent<Bullet>().damage;
                Invoke("Shoot", 0.5f);
            }
            else if (collision.gameObject.GetComponent<meeleeHit>())                //also damage when on meeleehit.
            {
                Instantiate(hitEffect, transform.position, transform.rotation);
                health -= collision.gameObject.GetComponent<meeleeHit>().damage;
                Invoke("Shoot", 2f);
            }           
            healthSlider.value = health;

            if (health <= 0) Death();
        }

        void Death()
        {
            manager.SlowMotion(0.4f, 1f);
            health = 0;
            Instantiate(deathEffect, transform.position, transform.rotation);
            dungeonManager.enemies.Remove(gameObject);
            if (dungeonManager.enemies.Count == 0) Instantiate(exit, transform.position, transform.rotation);
            Destroy(gameObject);
        }

        void Shoot()
        {
            Instantiate(bullet, bulletPosition.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + offset));
        }

    }

}
