﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class meeleeHit : MonoBehaviour              
    {
        public int damage;

        public float knockBack;

        [SerializeField]
        GameObject hitEffect;

        Transform follow;

        [SerializeField]
        bool parent = true, destroyOnContact = true;

        private void Start()
        {
            follow = GameObject.Find("GunReference").transform;
        }


        private void Update()
        {
            if (parent)
            {
                transform.position = follow.position;
                transform.rotation = follow.rotation;
            }
           
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.gameObject.tag == "Enemy")
            {              
                Instantiate(hitEffect, collision.gameObject.transform.position, transform.rotation);
                if(destroyOnContact) Destroy(gameObject);
            }
        }
    }

}

