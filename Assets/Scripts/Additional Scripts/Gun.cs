﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;                //for Ammotext
using UnityEngine.UI;       //for special slider
using UnityEngine.SceneManagement;              //For Debugging the "GameManagerBreaksTheGameAfterDying"-glitch. I know, great name.

namespace CoATwoPR
{
    public class Gun : MonoBehaviour
    {
        delegate void extraEffects();
        extraEffects extraeffects;                              //for Fast Disabling of Extra Effects

        public ScriptableGun scriptableGun;            //For assigning gun Values

        [HideInInspector]
        public Animator animator;           //referenced in meeleeWeapon to stop shooting anim

        CameraFollowing shake;

        GameManager gameManager;            //References to AmmunitionCounter     

        TextMeshProUGUI text;               //Ammo Text

        public GameObject meeleProjectile; //public so can be changed later
        GameObject projectile;             //Projectile, and the Meele "Projectile" and/or effect, which damages the enemy.

        
        GameObject smokeEffect;         //a Smokeffectr to add PEW for the first shot
        RaycastHit2D hit;               //For storing the point, where the smokeEffect has to be instanciated

        
        GameObject barrelSmoke;             //Smoke for Barrel

        GameObject magazine;           //Magazine to instantiate

        [SerializeField]
        LayerMask lay, enemyLayer;                  //Also for the Raycast

       
        float recoil = 1;

       
        bool fullAuto;                  //is The gun fully Automatic?

        
        int maxAmmo;                        //The Maximal amount of ammo

        [HideInInspector]
        public int additionalProjectiles, additionalAmmoUsage;

        
        bool closedBolt;                //MaxAmmo +1 reload mechanic?

        
        GameObject shell;

        
        AudioClip shootAudio, reloadAudio, reloadTactical, hotHissing, tail, emptyClick, safeClick;              //Audioclips for the audioSource
            
        
        float RPM = 300;

        Transform bullpo, casepo;                   //Bullet Position and Case Position, for Instantiating Projectiles and Shells/Cases

        float timer;

        [HideInInspector]
        public float internTimer;           //public for perks

        float specialInternTimer;

        [HideInInspector]
        public float specialTimer;

        Camera cam;

        float originalCamSize;                      //Reference to original Camera Size

        [HideInInspector]
        public AudioSource shot, firstshot, extraAudioSource;                //For extra Beef, the First shot should be louder and more extreme. "Shot" is referenced in MeeleeWeapon. The extraaudiosource is the hot hissing that can be heared and is referenced in the meeleeweapon script

        PlayerMovement move;      //used for recoil

        [HideInInspector]
        public SpriteRenderer spriteRenderer;                      //used to determine lookdirection for recoil. Referenced in Meelee

        public bool isSHooting;            //Active when shooting. Used to determine meele and referenced in Playermovement for SocailMode

        bool isMeeleing;        //Active when Meele. Used to determine shoot.

        [HideInInspector]
        public bool isReloading;       //used to determine if canShoot. Referenced in Playermovement for social mode

        int currentAmmo;               //The Ammo currently in the firearm

        bool lastRelease;               //UnShooting the gun One last time, when AvailableAmmo disables a reload.

        float originalVolume;           //Setting Shot.Volume back to its origin

        float reloadTime, tacReloadTime;                //reloadTimes

        bool canClick = true;              //Can click ONCE when ammo is empty

        float magazineInstantiateDelay;

        int amountOfProjectiles;

        [HideInInspector]
        public bool safe;

        public bool canbeUsed = true;

        bool usesMagazineAmmo;

        int ammoToAdd;      //for reloading calculations

        Slider slider;

        float originalPitch;            //for slowmotion pitch
        bool setPitchOnce;              //also for slowmotion pitch

        public GameObject muzzleFlash;      //public for meelee

        bool useFlash;

        RaycastHit2D meeleRayHit;

        bool rayCastHitsEnemy;          //decides of shoot or punch

        [SerializeField]
        GameObject shortMeeleHitter;

        [SerializeField]
        SpriteRenderer armSpriteRenderer;

        float enableSpritesTimer = 0.1f;

        bool isPunching;

        meeleeWeapon meeleeWeaponInstance;

        bool firstRefresh = true;

        void Start()
        {
            animator = GetComponent<Animator>();
            shake = FindObjectOfType<CameraFollowing>();
            bullpo = transform.GetChild(0);
            casepo = transform.GetChild(1);
            cam = Camera.main;
            move = FindObjectOfType<PlayerMovement>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            meeleeWeaponInstance = FindObjectOfType<meeleeWeapon>();

            originalCamSize = cam.orthographicSize;

            timer = (60 / RPM);

            shot = GetComponent<AudioSource>();

            firstshot = bullpo.GetComponent<AudioSource>();                 //FirstShot is alway part of the Bullet-Position

            extraeffects += CamZoom;

            currentAmmo = 1;

            text = GameObject.Find("AmmoText").GetComponent<TextMeshProUGUI>();

            extraAudioSource = transform.GetChild(1).GetComponent<AudioSource>();

            gameManager = FindObjectOfType<GameManager>();

            shot.loop = fullAuto;

            originalVolume = shot.volume;         

            text.text = currentAmmo + "/" + maxAmmo;

            slider = GameObject.Find("SpecialSlider").GetComponent<Slider>();

            changeValues();
        }

        public void gunSafety()
        {         
                if (!safe) safe = true;
                else safe = false;
                shot.Stop();
                shot.clip = safeClick;
                shot.loop = false;
                shot.pitch = Random.Range(0.95f, 1.05f);
                shot.Play();         
        }
       
        void Update()
        {
            SpriteEnabling();

            meeleRayHit = Physics2D.Raycast(transform.position, bullpo.right, 0.5f, enemyLayer);

            if (meeleRayHit.collider != null) rayCastHitsEnemy = true;
            else rayCastHitsEnemy = false;         

            // Debug.Log(lastRelease);
            if(StaticValues.isDead) if (muzzleFlash.activeInHierarchy) muzzleFlash.SetActive(false);            //no muzzleflash if dead
            if (canbeUsed)
            {
                if (!rayCastHitsEnemy)
                {
                    if (!safe)
                    {
                        SpecialAttack();
                        if (!isReloading)
                        {
                            if (!usesMagazineAmmo)
                            {
                                if (gameManager.ammoAvailable > 0) FullShootCheck();
                            }
                            else
                            {
                                if (currentAmmo > 0) FullShootCheck();
                            }



                        }
                        else if (Input.GetMouseButtonDown(0) && canClick && currentAmmo == 0 && !isReloading && !isSHooting)                                                                   //clicking once when empty, not reloading and not shooting
                        {
                            shot.clip = emptyClick;
                            shot.pitch = Random.Range(0.95f, 1.05f);
                            shot.Play();
                            shot.loop = false;

                            canClick = false;
                        }


                    }

                    if (usesMagazineAmmo)
                    {
                        if ((!isReloading && currentAmmo <= 0 && lastRelease))                       //Cannot be in the Void "ReloadCheck" because this has to be activated, even when player Input suggests shooting.
                        {
                            if (fullAuto) UnShoot();
                            if (gameManager.ammoAvailable > maxAmmo && usesMagazineAmmo) StartCoroutine(ReloadTac());

                            lastRelease = false;
                        }
                        if (!isSHooting) if (usesMagazineAmmo) ReloadCheck();
                    }
                    else
                    {
                        if (!isReloading && gameManager.ammoAvailable <= 0 && lastRelease)                      //Cannot be in the Void "ReloadCheck" because this has to be activated, even when player Input suggests shooting.
                        {
                            if (fullAuto) UnShoot();
                            if (gameManager.ammoAvailable > maxAmmo && usesMagazineAmmo) StartCoroutine(ReloadTac());

                            lastRelease = false;
                        }
                        if (!isSHooting) if (usesMagazineAmmo) ReloadCheck();
                    }

                    TimerMath();
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {                        
                        Invoke("Punch", 0.1f);
                    }
                }

                
            }
            else if (shot.isPlaying) shot.Stop();


            if (Time.timeScale < 1f)
            {
                if (!setPitchOnce)
                {
                    originalPitch = shot.pitch;
                    setPitchOnce = true;
                }

                shot.pitch = Time.timeScale;
            }
            else if (setPitchOnce)
            {
                shot.pitch = originalPitch;
                setPitchOnce = false;
            }
        }

        void SpriteEnabling()
        {
            if (!meeleeWeaponInstance.isAttacking)
            {
                if (isSHooting || isReloading || enableSpritesTimer > 0)
                {
                    if (!isPunching) spriteRenderer.enabled = true;
                    armSpriteRenderer.enabled = false;
                }
                else if (enableSpritesTimer <= 0 && !meeleeWeaponInstance.isAttacking)
                {
                    armSpriteRenderer.enabled = true;
                    spriteRenderer.enabled = false;
                }
            }
            else
            {
                armSpriteRenderer.enabled = false;
                spriteRenderer.enabled = false;
            }
           

            if (enableSpritesTimer > 0) enableSpritesTimer -= Time.deltaTime;
            else
            {
                enableSpritesTimer = 0;
            }
        }

        void Punch()
        {
            Enemy enem;
            if (!isSHooting && !isReloading && !isPunching)
            {
                spriteRenderer.enabled = false;
                shortMeeleHitter.SetActive(true);

                try
                {
                    enem = meeleRayHit.collider.GetComponent<Enemy>();      //else kick right


                    if ((transform.position.x - meeleRayHit.collider.gameObject.transform.position.x) < 0) enem.rigidBody2D.AddForce(Vector2.right * 200, ForceMode2D.Impulse);      //if left kick left
                    else enem.rigidBody2D.AddForce(Vector2.right * -200, ForceMode2D.Impulse);      //else kick right

                    enem.manualDamage(20, 0.1f, 0.1f);
                }
                catch
                {
                   // Debug.Log("no Enemy class attached");
                }

                isPunching = true;

                armSpriteRenderer.enabled = false;
                enableSpritesTimer = 0.3f;

                Invoke("UnPunch", 0.2f);
            }

           
        }

        void UnPunch()
        {
            isPunching = false;

            shortMeeleHitter.SetActive(false);        
        }

        void FullShootCheck()
        {
            if (isSHooting && !Input.GetMouseButton(0)) UnShoot();      //unshoot if still shooting but no input given

            if (fullAuto)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (!fullAuto)
                    {
                        shot.pitch = Random.Range(0.7f, 1.1f);
                        animator.SetTrigger("shoot");     //only one shot if not fullaut
                    }
                    else
                    {
                       if(useFlash) muzzleFlash.SetActive(true);
                    }
                    lastRelease = true;                        //LastRelease is possible
                    firstshot.Play();
                    StartCoroutine(shake.Shake(0.05f, 0.2f));

                    shot.clip = shootAudio;
                    shot.pitch = Random.Range(0.95f, 1.05f);
                    shot.Play();
                    shot.loop = true;
                    shot.volume = originalVolume;                  

                    hit = Physics2D.Raycast(bullpo.position, Vector3.up, -1.5f, lay);
                    if (hit.collider != null)
                    {
                        Instantiate(smokeEffect, hit.point, Quaternion.identity);
                    }

                    extraAudioSource.volume = 0;
                    extraAudioSource.clip = hotHissing;
                    extraAudioSource.loop = true;
                    extraAudioSource.Play();
                }

                if (Input.GetMouseButton(0) && internTimer <= 0)
                {
                    if (usesMagazineAmmo)
                    {
                        extraeffects();
                        Shoot();

                        if (!lastRelease)   //So the weapon can now properly unshoot. This is usually the case, when the player holds the left mouse button while reloading and after reloading. Because of this, this will trigger on the first shot of the new burst. Perfect for the firstshot audiosource.
                        {
                            firstshot.Play();

                            shot.clip = shootAudio;
                            shot.Play();
                            shot.loop = true;
                            shot.volume = originalVolume;

                            hit = Physics2D.Raycast(bullpo.position, Vector3.up, -1.5f, lay);           //the fristhot audio and raycast

                            if (hit.collider != null)
                            {
                                Instantiate(smokeEffect, hit.point, Quaternion.identity);
                            }

                            lastRelease = true;
                        }
                        isSHooting = true;
                    }
                    else
                    {
                        if (gameManager.ammoAvailable > 0)
                        {
                            extraeffects();
                            Shoot();

                            if (!lastRelease)   //So the weapon can now properly unshoot. This is usually the case, when the player holds the left mouse button while reloading and after reloading. Because of this, this will trigger on the first shot of the new burst. Perfect for the firstshot audiosource.
                            {
                                firstshot.Play();

                                hit = Physics2D.Raycast(bullpo.position, Vector3.up, -1.5f, lay);           //the fristhot audio and raycast

                                if (hit.collider != null)
                                {
                                    Instantiate(smokeEffect, hit.point, Quaternion.identity);
                                }

                                lastRelease = true;
                            }
                            isSHooting = true;
                        }
                    }                
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    UnShoot();
                }
            }
            else
            {
                if (usesMagazineAmmo)
                {
                    if (Input.GetMouseButtonDown(0) && internTimer <= 0)
                    {
                        lastRelease = true;
                        shot.clip = shootAudio;
                        Shoot();
                    }
                }
                else
                {
                    if (gameManager.ammoAvailable > 0)
                    {
                        if (Input.GetMouseButtonDown(0) && internTimer <= 0)
                        {
                            lastRelease = true;
                            shot.clip = shootAudio;
                            Shoot();
                        }
                    }
                }

            }
        }

        void TimerMath()
        {
            if (internTimer > 0) internTimer -= Time.deltaTime;             //seoerating special and gun
            else if (internTimer < 0) internTimer = 0;

            if (specialInternTimer > 0)
            {
                slider.value = specialInternTimer;          //also updating UI
                specialInternTimer -= Time.deltaTime;
            }
            else if (specialInternTimer < 0)
            {
                slider.value = specialInternTimer;
                specialInternTimer = 0;
            }
        }

        void ReloadCheck()
        {
            //if (gameManager.ammoAvailable >= maxAmmo - currentAmmo || gameManager.ammoAvailable > 0 && currentAmmo == maxAmmo)
            if (gameManager.ammoAvailable > 0 && !isReloading)
            {
                if (Input.GetKeyDown("r"))
                {
                    if (closedBolt)
                    {
                        if (currentAmmo < maxAmmo + 1 && currentAmmo != 0) StartCoroutine(Reload());
                        else if (currentAmmo == 0) StartCoroutine(ReloadTac());
                    }
                    else
                    {
                        if (currentAmmo < maxAmmo && currentAmmo != 0) StartCoroutine(Reload());
                        else if (currentAmmo == 0) StartCoroutine(ReloadTac());
                    }
                   

                }

            }

            if (currentAmmo == 0 && !canClick && shot.clip != emptyClick) canClick = true;                     //when empty the gun can click (once)
        }

        void Shoot()
        {
            CamZoom();

            internTimer = timer;

            if (!fullAuto)
            {
                shot.Stop();
                shot.pitch = Random.Range(0.9f, 1.1f);
                shot.pitch = Mathf.Clamp(shot.pitch, 0.7f, 1.5f);
                shot.PlayDelayed(0.01f);
            }
            else
            {
                if (!shot.isPlaying)
                {
                    shot.clip = shootAudio;
                    shot.Play();
                    shot.loop = true;
                }
            }

            if (extraAudioSource.volume < 0.7f) extraAudioSource.volume += 2 * Time.deltaTime;                //Making the Hiss louder

            if (fullAuto)
            {
                if (useFlash && !muzzleFlash.activeInHierarchy) muzzleFlash.SetActive(true);        //if button is held, it has to be set active again because of unshoot()
                animator.SetBool("isShooting", true);
            }
            else animator.SetTrigger("shoot");

            StartCoroutine(shake.Shake(0.05f, 0.1f));                       //ScreenShake

            enableSpritesTimer = 0.5f;

            for (int i = 0; i < amountOfProjectiles + additionalProjectiles; i++)
            {
                Instantiate(projectile, bullpo.position, transform.rotation);                               //the Shot
                Instantiate(shell, casepo.position, transform.rotation);
            }

            if (usesMagazineAmmo) currentAmmo -= 1 + additionalAmmoUsage;
            else
            {
                gameManager.ammoAvailable -= 1 + additionalAmmoUsage;
                if(gameManager.ammoAvailable > 0) gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;
                else
                {
                        gameManager.ammoAvailable = 0;          //to not get negative amounts of ammunition
                        gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;                   
                }
            }

            if(usesMagazineAmmo)text.text = currentAmmo + "/" + maxAmmo;        //Putting the string here will save performance.

            if (!move.walled)       //only add recoil if player is not walled. Otherwise player can "shoot" from walls by "delaying" recoil
            {
                if (spriteRenderer.flipY) move.extraForce += recoil;                                              //Recoil
                else move.extraForce -= recoil;
            }

            if (gameManager.ammoAvailable <= 0) gameManager.ammoAvailable = 0;          //to not get negative amounts of ammunition

        }

        void UnShoot()       
        {
            muzzleFlash.SetActive(false);

            animator.SetBool("isShooting", false);
            CamZoomBackToNormal();
            isSHooting = false;
            GameObject mySmoke = Instantiate(barrelSmoke, bullpo.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + 180));
           if(mySmoke.GetComponent<SpriteRenderer>()) mySmoke.GetComponent<SpriteRenderer>().flipY = !spriteRenderer.flipY;

            extraAudioSource.Stop();
            extraAudioSource.clip = tail;
           
            extraAudioSource.Play();
            
                shot.Stop();
                shot.loop = false;
                extraAudioSource.loop = false;
            
        }


        void CamZoom()
        {
            if (cam.orthographicSize >= originalCamSize * 0.8f) cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalCamSize * 0.8f, 0.2f);
        }

        void CamZoomBackToNormal()
        {
            while (cam.orthographicSize != originalCamSize)
            {
                cam.orthographicSize += Time.deltaTime * 2;

                if (Mathf.Abs(originalCamSize - cam.orthographicSize) <= 0.1f) cam.orthographicSize = originalCamSize;
            }
        }

        void SpecialAttack()                                                                                      //Meele Slash originally, I changed it to a special attack.
        {
            if(!isSHooting && Input.GetMouseButtonDown(2) && specialInternTimer == 0 && meeleProjectile != null)
            {              
                Instantiate(meeleProjectile, transform.position, transform.rotation);
                specialInternTimer = specialTimer;
                StartCoroutine(shake.Shake(0.05f, 0.1f));            //ScreenShake       

                slider.maxValue = specialTimer;         //has to be always set here, because there is not "refresh meele" method or similar.
            }           
        }


        IEnumerator Reload()
        {
            Invoke("SpawnMagazine", magazineInstantiateDelay);
            shot.Stop();
            shot.clip = reloadAudio;
            shot.loop = false;
            shot.pitch = Random.Range(0.9f, 1.1f);
            shot.Play();
            isReloading = true;
            animator.SetTrigger("reload");
            //isSHooting = true;                          //While the Player is not really shooting, the bool disallows the meeleeattack, thus making it perfect for the reload.
            
            if (closedBolt)
            {
                if (currentAmmo != maxAmmo)
                {
                    text.text = "Reloading";
                }
                else
                {
                    text.text = "Plus one'ing";
                }
            }
            else text.text = "Reloading";

            yield return new WaitForSeconds(reloadTime);

            if (gameManager.ammoAvailable > maxAmmo) ammoToAdd = maxAmmo - currentAmmo;
            else ammoToAdd = gameManager.ammoAvailable;

            if (closedBolt)
            {
                if (currentAmmo != maxAmmo)
                {
                    GameManagerAmmo();

                    currentAmmo += ammoToAdd + 1;
                    text.text = currentAmmo + "/" + maxAmmo;
                }
                else
                {
                    currentAmmo = maxAmmo + 1;
                    text.text = currentAmmo - 1 + "+1" + "/" + maxAmmo;

                    gameManager.ammoAvailable -= 1;
                    gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;                      //Same as gameManagerAmmo(), only with the subtraction of one
                }
            }
            else
            {
                GameManagerAmmo();

                currentAmmo += ammoToAdd;
                text.text = currentAmmo + "/" + maxAmmo;
            }
        
            if(currentAmmo > maxAmmo + 1)               //sometimes the firearm can reload more than necessary, oddly.
            {              
                gameManager.ammoAvailable += maxAmmo - currentAmmo;
                gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;

                currentAmmo = maxAmmo + 1;
            }
            shot.clip = shootAudio;
           // isSHooting = false;
            isReloading = false;

            if (gameManager.ammoAvailable <= 0) gameManager.ammoAvailable = 0;          //to not get negative amounts of ammunition
        }

        IEnumerator ReloadTac()
        {
            Invoke("SpawnMagazine", magazineInstantiateDelay);
            if (extraAudioSource.clip == hotHissing)
            {
                extraAudioSource.Stop();
                                            //Play the Tail, if still hissing!(has to be set, because mouse button can still be pressed)
                extraAudioSource.clip = tail;
                extraAudioSource.loop = false;
                extraAudioSource.Play();
            }                                      
            shot.Stop();
            shot.clip = reloadTactical;
            shot.pitch = Random.Range(0.9f, 1.1f);
            shot.loop = false;
            shot.Play();
            isReloading = true;
            animator.SetTrigger("tacReload");
            //isSHooting = true;                          //While the Player is not really shooting, the bool disallows the meeleeattack, thus making it perfect for the reload.
            text.text = "Full-Reload";

            yield return new WaitForSeconds(tacReloadTime);

            if (gameManager.ammoAvailable > maxAmmo) ammoToAdd = maxAmmo - currentAmmo;
            else ammoToAdd = gameManager.ammoAvailable;

            currentAmmo += ammoToAdd;

            text.text = currentAmmo + "/" + maxAmmo;
            shot.clip = shootAudio;
           // isSHooting = false;
            isReloading = false;

            GameManagerAmmo();


            if (currentAmmo > maxAmmo + 1)               //sometimes the firearm can reload more than necessary, oddly.
            {
                gameManager.ammoAvailable += maxAmmo - currentAmmo;
                gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;

                currentAmmo = maxAmmo + 1;
            }          

        }


        void GameManagerAmmo()
        {
            gameManager.ammoAvailable -= ammoToAdd;
            gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;
        }

        public void changeValues()                                                         //Change everything to scriptable Objects values. Public to be executed by Chests
        {
            if(muzzleFlash.activeInHierarchy) muzzleFlash.SetActive(false);
            useFlash = scriptableGun.useMuzzleFlash;
            projectile = scriptableGun.projectile;
            smokeEffect = scriptableGun.smokeEffect;
            barrelSmoke = scriptableGun.barrelSmoke;
            recoil = scriptableGun.recoil;
            fullAuto = scriptableGun.fullAuto;
            maxAmmo = scriptableGun.maxAmmo;
            closedBolt = scriptableGun.closedBolt;
            shell = scriptableGun.shell;
            shootAudio = scriptableGun.shootAudio;           
            reloadAudio = scriptableGun.reloadAudio;
            reloadTactical = scriptableGun.reloadTactical;
            hotHissing = scriptableGun.hotHissing;
            tail = scriptableGun.tail;
            RPM = scriptableGun.RPM;
            emptyClick = scriptableGun.emptyClick;
            magazine = scriptableGun.magazine;
            magazineInstantiateDelay = scriptableGun.magazineDelay;
            amountOfProjectiles = scriptableGun.amountOfProjectiles;
            safeClick = scriptableGun.safeClick;
            additionalAmmoUsage = scriptableGun.additionalAmmoUsage;
            usesMagazineAmmo = scriptableGun.usesMagazineAmmo;
            if (currentAmmo > maxAmmo) currentAmmo = maxAmmo;

            reloadTime = scriptableGun.reloadTime;
            tacReloadTime = scriptableGun.tacReloadTime;

            timer = (60 / RPM);     //timer has to be set again, to properly calculate the RPM

            animator.runtimeAnimatorController = scriptableGun.animator;

            if(SceneManager.GetActiveScene().name != "StartingRoom") gameManager.ammoAvailableText.text = "Left: " + gameManager.ammoAvailable;            //For some Reason, this Line of code breks the game if it is executed when teh startscene is loaded after dying. Whith this line of code only being necessarry, when a new weapon is picked up, it will only trigger outside the spawnroom.            
            if (usesMagazineAmmo)
            {
                if (!firstRefresh)
                {
                    gameManager.ammoAvailable += currentAmmo;
                    gameManager.RefreshAmmoText();
                    currentAmmo = 0;
                }            
                text.text = currentAmmo + "/" + maxAmmo;
            }
            else
            {
                if (!firstRefresh)
                {
                    gameManager.ammoAvailable += currentAmmo;
                    gameManager.RefreshAmmoText();
                    currentAmmo = 0;
                }
                currentAmmo = 0;
                text.text = " ";
            }

            firstRefresh = false;

        }
            
        public void SpawnMagazine()                 //Magazine Drop
        {
            Instantiate(magazine, casepo.position, transform.rotation);
        }

    }
}


