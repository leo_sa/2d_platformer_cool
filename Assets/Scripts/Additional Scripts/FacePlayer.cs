﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class FacePlayer : MonoBehaviour
    {
        [SerializeField]
        float offset;
        float rotZ;

        Transform player;

        void Start()
        {
            player = GameObject.Find("Player").transform;
        }

        void Update()
        {
            Vector2 difference = player.position - transform.position;
            rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);
        }
    }

}
