﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class HitSpark : MonoBehaviour
    {

        void Start()
        {
            if (transform.eulerAngles.z <= -100 && transform.eulerAngles.z >= -200 || transform.eulerAngles.z >= 100 && transform.eulerAngles.z <= 200)
            {
                GetComponent<SpriteRenderer>().flipY = true;
                GetComponent<SpriteRenderer>().flipX = true;
            }                                                                           //flip it to make ZOSH readable
            else
            {
                GetComponent<SpriteRenderer>().flipY = false;
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }

    }       //for Some Reason the rotation in the inspector is shown differently than the actual rotation. For example: The inspector says -2 degrees, while debug.log says 358 degrees. Thus the if-condition was developed through trial and error

}
