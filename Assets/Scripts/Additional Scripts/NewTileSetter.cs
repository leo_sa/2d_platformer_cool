﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class NewTileSetter : MonoBehaviour
    {
        [SerializeField]
        GameObject[] possibleTiles; 

        bool canSet = true;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            canSet = false;
        }

        void Start()
        {
            if (canSet && Vector2.Distance(transform.position, GameObject.Find("Player").transform.position) >= 15)
            {
                int n = Random.Range(0, 30);

                if (n <= 5) Instantiate(possibleTiles[Random.Range(0, possibleTiles.Length - 1)], transform.position, Quaternion.identity);
            }
        }     

    }

}
