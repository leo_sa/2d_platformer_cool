﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;            // for Text

namespace CoATwoPR
{
    public class MeeleeWeaponPickup : MonoBehaviour
    {
        bool interact;

        bool canPickup;

        meeleeWeapon weapon;

        SpriteRenderer spriteRenderer;

        [SerializeField]
        scriptableMeeleeWeapon meeleeWeaponData;

        scriptableMeeleeWeapon temporaryMeeleeData;                                     //to change gunData via script on both sides, it has to be stored temporarly

        TextMeshPro text;

        void Start()
        {
            weapon = FindObjectOfType<meeleeWeapon>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            text = transform.GetChild(1).GetComponent<TextMeshPro>();         //setting the Reference to the second child Component
            text.text = meeleeWeaponData.weaponName;
            text.enabled = false;               //will probbaly spawn outside of player-reach, so text should not be visible
        }


        void Update()
        {
            if (interact) PickUpWeapon();

            if (canPickup)
            {
                if (Input.GetKeyDown("e")) interact = true;
                else interact = false;
            }
        }

        void PickUpWeapon()
        {
            temporaryMeeleeData = weapon.weapon;
            weapon.weapon = meeleeWeaponData;
            meeleeWeaponData = temporaryMeeleeData;

            if(meeleeWeaponData.itemSprite != null) spriteRenderer.sprite = meeleeWeaponData.itemSprite;
            else spriteRenderer.sprite = meeleeWeaponData.worldSprite;

            text.text = meeleeWeaponData.weaponName;                            //name text has to be refreshed

            weapon.refreshStats();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = true;                                //Only enabled when player is next to it, to increase immersion/not fully kill it.
                canPickup = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = false;
                canPickup = false;
            }
        }

    }
}

