﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class InstantiateOnCollision : MonoBehaviour
    {
        [SerializeField]
        GameObject effect;

        float hitTimer;

        private void Update()
        {
            if (hitTimer > 0) hitTimer -= Time.deltaTime;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (hitTimer <= 0)
            {
                Instantiate(effect, collision.GetContact(0).point, transform.rotation);
                hitTimer = 0.1f;
            }
        }
    }

}
