﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;           //for Special_UI

namespace CoATwoPR
{
    public class MeelePickup : MonoBehaviour            //copied from WeaponPickup. "SpecialPickup" would be a more appropriate name
    {
        bool interact;

        bool canPickup;

        Gun gun;

        SpriteRenderer spriteRenderer;

        Image image;

        [SerializeField]
        GameObject meelePrefab;

        [SerializeField]
        float timer;

        GameObject temporaryMeelePrefab;                                     //to change meele via script on both sides, it has to be stored temporarly

        TextMeshPro text;

        [SerializeField]
        string customName;

        void Start()
        {
            gun = FindObjectOfType<Gun>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            text = transform.GetChild(1).GetComponent<TextMeshPro>();         //setting the Reference to the second child Component

            if(customName == null) text.text = meelePrefab.name;
            else text.text = customName;

            text.enabled = false;               //will probbaly spawn outside of player-reach, so text should not be visible

            image = GameObject.Find("SpecialImage").GetComponent<Image>();
        }


        void Update()
        {
            if (interact) PickUpWeapon();

            if (canPickup)
            {
                if (Input.GetKeyDown("e")) interact = true;
                else interact = false;
            }
        }

        void PickUpWeapon()
        {
            image.sprite = meelePrefab.GetComponent<SpriteRenderer>().sprite;
            if(gun.meeleProjectile != null) temporaryMeelePrefab = gun.meeleProjectile;
            gun.meeleProjectile = meelePrefab;
            gun.specialTimer = timer;
            timer = gun.specialTimer;
            if (temporaryMeelePrefab != null) meelePrefab = temporaryMeelePrefab;
            if (temporaryMeelePrefab != null) spriteRenderer.sprite = temporaryMeelePrefab.GetComponent<SpriteRenderer>().sprite;

            if (customName == null) text.text = meelePrefab.name;
            else text.text = customName;            //name has also to be refreshed

            if (temporaryMeelePrefab == null) Destroy(gameObject);          //will only be null on first pickup, so it can be destroyed.
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = true;                                //Only enabled when player is next to it, to increase immersion/not fully kill it.
                canPickup = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = false;
                canPickup = false;
            }
        }
    }
}

