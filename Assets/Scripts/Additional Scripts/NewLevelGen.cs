﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class NewLevelGen : MonoBehaviour
    {
        DungeonManager manager;

        [SerializeField]
        GameObject horizontalTiles, verticalTiles, saverOR, saverUR, saverOL, saverUL, levelCloserStart, levelCloserDir1, levelCloserDir2, levelCloserDir3, bossSPawnPoint, spawnerCluster;

        [SerializeField]
        int possibleSteps = 40;
        int dirY;
        int currentDir;

        [SerializeField]
        float distanceBetweenTiles;

        bool goUpwards;

        void Start()
        {
            manager = FindObjectOfType<DungeonManager>();
            Instantiate(levelCloserStart, transform.position, Quaternion.identity);
            currentDir = 1;
        }


        void Update()
        {
            if (possibleSteps > 0)
            {
                if (!goUpwards) placeVerticalTile();
                else placeHorizontalTile();

                if (possibleSteps % 10 == 0 && possibleSteps > 10)
                {
                    changeDirection();
                }
                else if (possibleSteps % 20 == 0)
                {
                    //Instantiate(spawnerCluster, transform.position, Quaternion.identity);         CUT_UNUSDED CONTENT
                }
            }

            if (possibleSteps == 0)
            {
                switch (currentDir)
                {
                    case 1:
                        Instantiate(levelCloserDir1, transform.position, Quaternion.identity);
                        break;

                    case 2:
                        Instantiate(levelCloserDir2, transform.position, Quaternion.identity);
                        break;

                    case 3:
                        Instantiate(levelCloserDir3, transform.position, Quaternion.identity);
                        break;
                }               
                manager.canChange = true;           //is changed only once
                possibleSteps = -1;
                Destroy(gameObject);            //Not needed anymore
            }
            else if (possibleSteps == distanceBetweenTiles / 2) Instantiate(bossSPawnPoint, transform.position, transform.rotation);

        }

        void placeVerticalTile()
        {
            Instantiate(verticalTiles, transform.position, Quaternion.identity);
            transform.position += new Vector3(1, 0);
            possibleSteps--;
        }

        void placeHorizontalTile()
        {
            Instantiate(horizontalTiles, transform.position, Quaternion.identity);
            transform.position += new Vector3(0, dirY);
            possibleSteps--;
        }

        void changeDirection()
        {
            int n = Random.Range(0, 2);
            if (n == 1)
            {
                int n2 = Random.Range(1, 3);
                switch (currentDir)
                {
                    case 1:
                        if (n2 == 2) changeDir(2);
                        else changeDir(3);

                        break;

                    case 2:
                        changeDir(1);
                        break;

                    case 3:
                        changeDir(1);
                        break;
                }

            }
        }

        void changeDir(int dirToGo)
        {
            switch (dirToGo)
            {
                case 1:

                    goUpwards = false;
                    if (currentDir == 2)
                    {
                        Instantiate(saverOL, transform.position, transform.rotation);
                        transform.position += new Vector3(distanceBetweenTiles, distanceBetweenTiles);
                    }
                    else if (currentDir == 3)
                    {
                        Instantiate(saverUL, transform.position, transform.rotation);
                        transform.position += new Vector3(distanceBetweenTiles, -distanceBetweenTiles);
                    }

                    break;

                case 2:

                    goUpwards = true;
                    transform.position += new Vector3(distanceBetweenTiles, distanceBetweenTiles);
                    Instantiate(saverUR, transform.position, transform.rotation);
                    dirY = 1;

                    break;

                case 3:

                    goUpwards = true;
                    transform.position += new Vector3(distanceBetweenTiles, -distanceBetweenTiles);
                    Instantiate(saverOR, transform.position, transform.rotation);
                    dirY = -1;

                    break;
            }

            currentDir = dirToGo;
        }

    }

}
