﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;        //for Damage Numbers

namespace CoATwoPR
{

    public class Enemy : MonoBehaviour
    {
        [SerializeField]
        GameObject damageNumber;

        [SerializeField]
        int health;

        [Range(0, 1)]
        public float weight;          //for Grappling Hook. If 10 then the Player will go in this direction.

        [HideInInspector]
        public float forceX, forceY;            //also for grappling Hook

        [SerializeField]
        bool hasDamageAnim;

        [SerializeField]
        bool addToEnemyList = true;

        Animator animator;              //only if it has damage animation

        [SerializeField, Range(0, 1)]
        float chanceForSlowMotionDeath;

        GameManager gameManager;            //referenced for slowMotionDeath
        CameraFollowing shake;          //for screenShake

        [SerializeField]
        bool dummy;                     //For infinite Health
        [SerializeField]
        bool dieOnPlayerCollision;           //especially for chests
        bool deadEffectSpawned;      //bool to only spawn effect once

        [SerializeField]
        GameObject hurtEffect, deathEffect;

        SpriteRenderer spriteRenderer;

        [HideInInspector]
        public Rigidbody2D rigidBody2D;     //public for punch(gun)

        float normalColorTimer;

        DungeonManager dun;         //For referencing the Enemies list.

        meeleeHit melHit;           //storing as a variable, to save some getComponent calls.

        [SerializeField, Range(0, 1)]
        float[] chances;                            //Chances for the items do drop, parralel

        [SerializeField]
        int[] amountToDrop;                     //amount of Items to drop

        [SerializeField]
        GameObject[] itemsToDrop;                       //Items to Drop

        float n;            //For messing the chance

        [SerializeField, Range(0, 1)]
        float slowMotionTime;


        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            dun = FindObjectOfType<DungeonManager>();
            if(!dummy && addToEnemyList) dun.enemies.Add(gameObject);
            rigidBody2D = GetComponent<Rigidbody2D>();
            gameManager = FindObjectOfType<GameManager>();              //can be found simply, because of the singleton concept.
            if (hasDamageAnim) animator = GetComponent<Animator>();
            shake = FindObjectOfType<CameraFollowing>();
        }


        void Update()
        {
            if (transform.position.y <= -500) die();            //if out of bounds
            colorTimer();

            if(health <= 0)
            {

                if (dummy) health = 9999999;
                else
                {
                    health = 0;
                    die();
                }

            }
            DecreaseForce();
        }
        
        void DecreaseForce()
        {
            if (forceX != 0) forceX = Mathf.Lerp(forceX, 0, 0.01f);          //decreasing force
            if (forceY != 0) forceY = Mathf.Lerp(forceY, 0, 0.01f);

            if (Mathf.Abs(forceX) <= 1f) forceX = 0;
            if (Mathf.Abs(forceY) <= 1f) forceY = 0;
        }

        private void FixedUpdate()
        {
            if (forceY != 0 || forceX != 0) rigidBody2D.velocity = new Vector2(forceX * 2, forceY * 2);
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {

            if (collision.gameObject.GetComponent<Bullet>())                    
            {             
                manualDamage(collision.gameObject.GetComponent<Bullet>().damage, 0, 1);
            }
            else if (rigidBody2D.velocity.magnitude >= 10)
            {
                SlowMotion(0.04f, 0.2f);
                StartCoroutine(shake.Shake(0.1f, 0.1f));
                manualDamage((int)rigidBody2D.velocity.magnitude, 0.01f, 0.5f);              //if enemy crashing fast against a wall, he will take damage;
            }
            else if (dieOnPlayerCollision)
            {
                if (collision.gameObject.name == "Player") manualDamage(health, 0, 1);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<Bullet>())
            {
                manualDamage(collision.gameObject.GetComponent<Bullet>().damage, 0, 1);               
            }
            else if (collision.gameObject.GetComponent<meeleeHit>())                //also damage when on meeleehit.
            {
                SlowMotion(0.005f, 0.1f);
                melHit = collision.gameObject.GetComponent<meeleeHit>();
                manualDamage(melHit.damage, 0.15f, 0.5f);

                rigidBody2D.AddForce((transform.position - melHit.transform.position) * melHit.knockBack * (1 - weight), ForceMode2D.Impulse);
                
            }
        }


        public void manualDamage(int damage, float slowmotionLength, float slowmotionIntensity)
        {
            GameObject instanceOfDamageNumber = Instantiate(damageNumber, transform.position, transform.rotation);
            instanceOfDamageNumber.GetComponent<TextMeshPro>().text = damage.ToString();            //Damage Number
            if (animator) animator.SetTrigger("hurt");
            health -= damage;
            Instantiate(hurtEffect, transform.position, transform.rotation);            //Spawns effect at transform Position
            spriteRenderer.color = Color.red;
            normalColorTimer = 0.1f;
           
            SlowMotionCheck(slowmotionLength, slowmotionIntensity);
        }

        void colorTimer()               //How long does the enemy stay red?
        {
            if (normalColorTimer > 0) normalColorTimer -= Time.deltaTime;
            else if (normalColorTimer < 0)
            {
                spriteRenderer.color = Color.white;
                normalColorTimer = 0;
            }
        }

        public void die()
        {
            
            if(addToEnemyList) if (dun.enemies.Count == 1) Instantiate(dun.dungeonExit, transform.position, Quaternion.identity);
            if (!deadEffectSpawned)
            {
                Instantiate(deathEffect, transform.position, Quaternion.identity);
                deadEffectSpawned = true;
            }
            if(addToEnemyList) dun.enemies.Remove(gameObject);

            for (int i = 0; i < itemsToDrop.Length; i++)                                                                    // a small algorythm for dropping the Items
            {
                n = Random.Range(0, 100);
                for (int x = 0; x < amountToDrop[i]; x++)
                {
                    if (n <= (chances[i] * 100)) Instantiate(itemsToDrop[i], transform.position, transform.rotation);
                }
               
            }

            int n2;
            n2 =Random.Range(0, 100);
            if (n2 <= 30 && chanceForSlowMotionDeath != 0)  //30 % chance for slowmotion  if chance not zero  
            {
                //shake.starZooming(transform.position);        //if slowmotion do extra zoom, currently bugged and cut because it places the camera falseley and intereferes with the gun.
                SlowMotion(0.05f, 0.2f);                
            }
            Destroy(gameObject);
        }

        void SlowMotionCheck(float slomoTime, float slomoIntensity)
        {
           
            if(chanceForSlowMotionDeath != 0)
            {
                n = Random.Range(0, 100);
                if (n <= (chanceForSlowMotionDeath * 100))
                {
                    SlowMotion(slomoTime, slomoIntensity);
                }
            }
            
        }

        void SlowMotion(float length, float intensity)
        {
            gameManager.canSetTime = false;

            gameManager.slowMotionTime = length;

            gameManager.SlowMotion(intensity, length);
        }
        
    }
}