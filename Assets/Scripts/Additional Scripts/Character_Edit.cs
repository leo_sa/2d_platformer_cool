﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace CoATwoPR
{
    public class Character_Edit : MonoBehaviour
    {
        [SerializeField]
        RuntimeAnimatorController[] characters;

        [SerializeField]
        GameObject[] voices;

        [SerializeField]
        Sprite[] hairSprites;

        [SerializeField]
        SpriteRenderer[] hairSpriteRenderers;

        [SerializeField]
        TextMeshProUGUI voiceText, nameText;

        int characterIndex, voiceIndex;

        Animator playerAnim;
        PlayerHealth playerHurt;

        void Start()
        {
            playerAnim = GameObject.Find("Player").GetComponent<Animator>();
            playerHurt = FindObjectOfType<PlayerHealth>();
            characterIndex = PlayerPrefs.GetInt("characterIndex");
            voiceIndex = PlayerPrefs.GetInt("voiceIndex");

            UpdateCharacter();
        }


        public void addVoiceIndex()
        {
            if(voiceIndex < voices.Length - 1) voiceIndex++;                //minus one has to be added, because of the way arrays are counted
            UpdateCharacter();
        }

        public void subtractVoiceIndex()
        {
            if (voiceIndex > 0) voiceIndex--;
            UpdateCharacter();
        }

        public void addCharacterIndex()
        {
            if (characterIndex < characters.Length - 1) characterIndex++;
            UpdateCharacter();
        }

        public void subtractCharacterIndex()
        {
            if (characterIndex > 0) characterIndex--;
            UpdateCharacter();
        }


        void UpdateCharacter()
        {
            PlayerPrefs.SetInt("characterIndex", characterIndex);
            PlayerPrefs.SetInt("voiceIndex", voiceIndex);
            PlayerPrefs.Save();

            nameText.text = characters[characterIndex].name;
            voiceText.text = voices[voiceIndex].name;

            playerAnim.runtimeAnimatorController = characters[characterIndex];
            playerHurt.HurtEffect = voices[voiceIndex];

            for (int i = 0; i < hairSpriteRenderers.Length; i++)
            {
                hairSpriteRenderers[i].sprite = hairSprites[characterIndex];
            }           
        }

    }
}

