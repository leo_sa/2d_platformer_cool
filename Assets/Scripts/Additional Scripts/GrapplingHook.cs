﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{    
    public class GrapplingHook : MonoBehaviour2          //some code copied from the "ActualRay" class.
    {
        [SerializeField]
        float moveSpeed, grabSpeed;

        bool goTo, getOver;         //Player goes to grabbed object OR grabbed Objects gets to player.
        bool grabbed;

        Rigidbody2D body2D;

        LineRenderer line;

        Transform player, enemy;

        PlayerMovement move;

        Enemy grabbedEnemy;

        [SerializeField]
        GameObject hitEffectEnemy;

        [SerializeField]
        AudioClip grabbedClip;
        AudioSource audioSource;

        void Start()
        {
            line = GetComponent<LineRenderer>();
            player = GameObject.Find("Player").transform;
            body2D = GetComponent<Rigidbody2D>();
            move = FindObjectOfType<PlayerMovement>();
            audioSource = GetComponent<AudioSource>();

        }


        void Update()
        {
            if (Input.GetMouseButton(2))
            {
                body2D.velocity = transform.right * moveSpeed;
            }
            else if (Input.GetMouseButtonUp(2)) Stop();
            float distance = Vector2.Distance(transform.position, -player.transform.position);

            line.SetPosition(0, transform.position);
            line.SetPosition(1, player.transform.position);

            if (goTo)
            {
                move.grapForceX = (transform.position.x - player.position.x) * grabSpeed;
                move.grapForceY = (transform.position.y - player.position.y) / 3;           //has to be decreased, because in playermovement, the Y axis is also multiplied with the velocity aka with itself.              
            }
            else if (getOver)
            {
                grabbedEnemy.forceX = (player.position.x - enemy.position.x) * grabSpeed * 5 * (1 - grabbedEnemy.weight);           // the five is just extra speed
                grabbedEnemy.forceY = (player.position.y - grabbedEnemy.transform.position.y) * grabSpeed * 5 * (1 - grabbedEnemy.weight); 
            }

            DistanceCheck();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Instantiate(hitEffectEnemy, collision.GetContact(0).point, transform.rotation);
            MyPlay(audioSource, grabbedClip);

            if (collision.gameObject.GetComponent<Enemy>())
            {
                grabbedEnemy = collision.gameObject.GetComponent<Enemy>();
                grabbedEnemy.manualDamage(5, 0.05f, 0.1f);
                enemy = collision.gameObject.transform;              

                if (grabbedEnemy.weight < 1) getOver = true;                //if enemy is light, he comes to the player, otherwise the player goes to him
                else goTo = true;

                transform.parent = collision.gameObject.transform;
                Freeze();

            }
            else
            {
                Freeze();
                goTo = true;
            }
            grabbed = true;
        }

        void DistanceCheck()                //stop if too close when grabbed or when distance is too big
        {
            if (grabbed)
            {
                if (Vector2.Distance(transform.position, player.position) <= 0.5f) Stop();
                if (getOver) if (Vector2.Distance(player.position, grabbedEnemy.transform.position) <= 0.5f) Stop();
            }         
            if (Vector2.Distance(transform.position, player.position) >= 10f) Stop();

        }

        void Stop()
        {          
            Destroy(gameObject);
        }

        void Freeze()
        {
            GetComponent<Collider2D>().enabled = false;
            body2D.isKinematic = true;
            body2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            moveSpeed = 0;
        }

    }

}
