﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class DamageEnemy : MonoBehaviour
    {
        [SerializeField]
        int damage;

        [SerializeField]
        float slowmotionLength, slowMotionIntensity;
       
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<Enemy>()) collision.gameObject.GetComponent<Enemy>().manualDamage(damage, slowmotionLength, slowMotionIntensity);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Enemy>()) collision.gameObject.GetComponent<Enemy>().manualDamage(damage, slowmotionLength, slowMotionIntensity);
        }
    }

}
