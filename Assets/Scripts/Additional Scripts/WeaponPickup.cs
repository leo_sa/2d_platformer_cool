﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;                // for GunName

namespace CoATwoPR
{
    public class WeaponPickup : MonoBehaviour
    {
        bool interact;

        bool canPickup;

        Gun gun;

        SpriteRenderer spriteRenderer;

        [SerializeField]
        ScriptableGun gunData;
        
        ScriptableGun temporaryGunData;                                     //to change gunData via script on both sides, it has to be stored temporarly

        TextMeshPro text;

        void Start()
        {
            gun = FindObjectOfType<Gun>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            text = transform.GetChild(1).GetComponent<TextMeshPro>();         //setting the Reference to the second child Component
            text.text = gunData.name;
            text.enabled = false;               //will probbaly spawn outside of player-reach, so text should not be visible
        }


        void Update()
        {
            if (interact) PickUpWeapon();

            if (canPickup)
            {
                if (Input.GetKeyDown("e")) interact = true;
                else interact = false;
            }
        }

        void PickUpWeapon()
        {           
            temporaryGunData = gun.scriptableGun;
            gun.scriptableGun = gunData;
            gunData = temporaryGunData;
            spriteRenderer.sprite = gunData.worldSprite;
            text.text = gunData.name;              //name has also to be refreshed

            gun.changeValues();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = true;                                //Only enabled when player is next to it, to increase immersion/not fully kill it.
                canPickup = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                text.enabled = false;
                canPickup = false;
            }
        }

    }
}

