﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class PerkDealer : MonoBehaviour
    {
        bool mayInteract;
        bool setOnce, setBackOnce;

        GameObject perkUI;


        GameManager gManager;
        Gun gun;
        PlayerMovement playerMove;

        void Start()
        {

            perkUI = transform.GetChild(0).GetChild(0).gameObject;
            perkUI.SetActive(false);

            gManager = FindObjectOfType<GameManager>();
            gun = FindObjectOfType<Gun>();
            playerMove = FindObjectOfType<PlayerMovement>();

        }


        void Update()
        {
            if (mayInteract)
            {
                if (Input.GetKeyDown("e") && !setOnce)
                {
                    if (!perkUI.activeInHierarchy) perkUI.SetActive(true);

                    gManager.canSetTime = false;                //copied from NPC. Setting Game into "pause-mode".
                    gManager.canPause = false;
                    Time.timeScale = 0.01f;
                    Time.fixedDeltaTime = 0.02f * Time.timeScale;
                    gun.canbeUsed = false;
                    playerMove.canMove = false;

                    setOnce = true;
                }
            }

            if (setOnce)
            {
                if (!setBackOnce)
                {
                    if (!perkUI.activeInHierarchy)
                    {
                        gManager.canSetTime = true;                //copied from NPC. Setting Game back to normal.
                        gManager.canPause = true;
                        Time.timeScale = 1;
                        Time.fixedDeltaTime = 0.02f * Time.timeScale;
                        gun.canbeUsed = true;
                        playerMove.canMove = true;

                        setBackOnce = true;
                    }
                }
               
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                mayInteract = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                mayInteract = false;
            }
        }
    }
}

