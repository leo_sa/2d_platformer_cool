﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;            //for showing Item_Name

namespace CoATwoPR
{
    public class Item : MonoBehaviour
    {
        TextMeshPro text;

        bool canPickUp;
        bool picked;

        [SerializeField]
        scriptableItem item;

        ItemContainer[] container;          //all containers

        void Start()
        {
            container = GameObject.Find("Inventory").transform.GetComponentsInChildren<ItemContainer>();
            text = transform.GetChild(0).GetComponent<TextMeshPro>();
        }


        void Update()
        {
            if (canPickUp)
            {
                if (Input.GetKeyDown("e")) PickItem();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                canPickUp = true;
                text.text = item.itemName;
            }

        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                canPickUp = false;
                text.text = null;
            }
        }

        void PickItem()
        {
            for (int i = 0; i < container.Length; i++)
            {
                if (!picked)
                {
                    if (!container[i].holdsItem)
                    {
                        container[i].addItem(item);
                        picked = true;
                    }
                }
            }
            if (picked) Destroy(gameObject);
        }
    }

}
