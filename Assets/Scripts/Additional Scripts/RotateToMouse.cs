﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class RotateToMouse : MonoBehaviour    
    {
        float rotZ;

        [Range(0, 360)]
        int offset;

        void Update()
        {
            Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;                                                         //The Tangent, which leads to the MousePos, is Calculated to radians and converted intro degrees, for EULER-Usage.

            transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);
        }
    }
}

