﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class ShootPlayer : MonoBehaviour
    {
        Transform player;
        Transform bulletPosition;               //aka the BullPo


        AudioSource audioSource;


        SpriteRenderer spriteRenderer;

        [SerializeField]
        LayerMask lay;

        [SerializeField]
        GameObject bullet;


        RaycastHit2D hit2D;


        [SerializeField, Range(0, 360)]
        int offset;       
        [SerializeField]
        int projectileAmount = 1;

        [SerializeField]
        float maxDistance = 5;
        [SerializeField, Range(1, 3000)]
        float RPM;
        [SerializeField, Range(0, 360)]
        float accuracy;

        float rotZ;
        float internTimer;
        float maxTimer;


        private void Start()
        {
            bulletPosition = transform.GetChild(0).transform;
            player = GameObject.Find("Player").transform;
            audioSource = GetComponent<AudioSource>();
            spriteRenderer = GetComponent<SpriteRenderer>();

            maxTimer = (60 / RPM);
            internTimer = maxTimer;
        }

        private void Update()
        {
            if(Vector2.Distance(transform.position, player.position) <= maxDistance)
            {
                hit2D = Physics2D.Raycast(bulletPosition.position, transform.right, maxDistance, lay);
                if(hit2D.collider != null) if (hit2D.collider.gameObject.layer != 11) GunCheck();             //shot only when not looking at tile. Or in other words: Only shooting when it can look at player. Currently the layer is set to none, because the gameobject will not shoot when a tile is behind the player
                Rotate();
            }
        }


        void Rotate()               //basically RotateToMouse but with the player
        {
            Vector2 difference = player.position - transform.position;
            rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;                                                         //The Tangent, which leads to the PlayerPos, is Calculated into radians and converted intro degrees, for EULER-Usage.

            transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);

            if ((transform.position.x - player.position.x) < 0) spriteRenderer.flipY = false;
            else spriteRenderer.flipY = true;
        }

        void GunCheck()
        {
            if (internTimer > 0) internTimer -= Time.deltaTime;
            else if (internTimer < 0) Shoot();
        }

        void Shoot()
        {
            audioSource.Stop();
            audioSource.pitch = Random.Range(0.95f, 1.05f);
            audioSource.PlayDelayed(0.01f);         //delaying to put more emphasis on the shot sound.

            internTimer = maxTimer;

            for (int i = 0; i < projectileAmount; i++) Instantiate(bullet, bulletPosition.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + Random.Range(-accuracy / 2, accuracy / 2)));                    
        }
    }
}

