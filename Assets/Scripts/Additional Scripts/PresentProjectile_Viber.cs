﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class PresentProjectile_Viber : MonoBehaviour
    {
        Animator animator;

        [SerializeField]
        float speed = 0.1f;             //serialized Field, so I can set in inspector, if the speed is negative or positive

        int currentTriggers;

        [SerializeField]
        LayerMask lay;

        void Start()
        {
            animator = GetComponent<Animator>();
        }


        void Update()
        {
            transform.Translate(transform.right * speed);
        }

        private void FixedUpdate()
        {
            if (Physics2D.Raycast(transform.position, transform.right, speed, lay)) Kill();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            currentTriggers++;

            if(currentTriggers == 0)
            {
                speed = 0;
                animator.SetTrigger("end");
                Invoke("Kill", 0.1f);
            }
            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            currentTriggers--;
        }


        void Kill()
        {
            Destroy(gameObject);
        }
     
    }

}
