﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public static class StaticValues
    {
        [Range (-1000, 100)]
        public static float respect;

        public static bool isDead, canCountTime;


        public static void resetValues()
        {
            respect = 0;
            isDead = false;
        }
    }

}
