﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class LightFlickering : MonoBehaviour
    {
        Light light;

        [SerializeField]
        float intensity;

        float start;
        float localIntensity;

        [Range(0, 1)]
        public float lerpSpeed = 0.2f;

        void Start()
        {
            light = GetComponent<Light>();
            start = light.intensity;
        }


        void Update()
        {
            localIntensity = Random.Range(start - intensity, start + intensity);

            light.intensity = Mathf.Lerp(light.intensity, localIntensity, lerpSpeed);
            light.range = Mathf.Lerp(light.intensity, localIntensity, lerpSpeed);

        }

    }

}
