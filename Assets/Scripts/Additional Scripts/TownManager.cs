﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class TownManager : MonoBehaviour
    {
        TextMeshProUGUI reputationNumber;

        HonestCardManager cardManager;      //to check for badDeedCards       

        private void Start()
        {
            cardManager = FindObjectOfType<HonestCardManager>();
            
            FindObjectOfType<HonestManager>().liesLeft = 7;
          
            reputationNumber = transform.GetComponentInChildren<TextMeshProUGUI>();
            reputationNumber.text = "Reputation:" + StaticValues.respect;

            if(cardManager.numberOfBadDeedCards > 0)
            {
                for (int i = 0; i < cardManager.numberOfBadDeedCards; i++)
                {
                    int n = Random.Range(0, 100);
                    if(n <= 25)
                    {
                        cardManager.PlayBadDeed();
                    }
                    else
                    {
                        cardManager.badDeedsHeldIn[i - 1]++;
                        cardManager.potentialDecreases[i - 1] += 5 * cardManager.badDeedsHeldIn[i - 1];
                    }
                }
            }
        }
      
        public void DecreaseRespect(int decreaseNumber)
        {
            StaticValues.respect -= decreaseNumber;
            RefreshUI();
        }

        public void IncreaseRespect(int increaseNumber)
        {
            StaticValues.respect += increaseNumber;
            RefreshUI();
        }

        void RefreshUI()
        {
            reputationNumber.text = "Reputation:" + StaticValues.respect;
        }      

    }

}
