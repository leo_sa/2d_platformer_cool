﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class AgressiveJojo : MonoBehaviour
    {
        GameObject rotationPivot, bullPo;           //A bullPo reference is needed to throw it with consistent speed to the cam-position.

        [SerializeField]
        AudioClip shootClip;

        AudioSource shotSource;

        Vector3 zAxis = new Vector3(0, 0, 1);

        Vector3 mousePos;

        Rigidbody2D body2D;

        float rotationSpeed = 180, moveSpeed;

        bool fullAccelerated, canAccelerate = true, addForceOnce, endTheAttack;

        

        void Start()
        {
            shotSource = GetComponent<AudioSource>();
            body2D = GetComponent<Rigidbody2D>();
            rotationPivot = GameObject.Find("Player");
            bullPo = GameObject.Find("bulPo");

            Invoke("PlayAudio", 0.1f);      //playing the loop with delay

            //transform.parent = rotationPivot.transform;
        }

        void PlayAudio()
        {
            shotSource.Play();
            shotSource.loop = true;
        }

        
        void Update()
        {
            if (canAccelerate)
            {
                Acceleration();

                transform.RotateAround(rotationPivot.transform.position, zAxis, rotationSpeed * Time.deltaTime);

                if (!fullAccelerated && Vector2.Distance(transform.position, rotationPivot.transform.position) <= 2) transform.position = Vector2.MoveTowards(transform.position, rotationPivot.transform.position, -1 * moveSpeed * Time.deltaTime);       //going away for around 2 Units
                else
                {
                    if (!fullAccelerated) fullAccelerated = true;
                    if (Vector2.Distance(transform.position, rotationPivot.transform.position) >= 1) transform.position = Vector2.MoveTowards(transform.position, rotationPivot.transform.position, 1 * moveSpeed * Time.deltaTime);            //going back to around 1 Unit
                    else
                if (!fullAccelerated && Vector2.Distance(transform.position, rotationPivot.transform.position) <= 1) transform.position = Vector2.MoveTowards(transform.position, rotationPivot.transform.position, -1 * moveSpeed * Time.deltaTime);       //It needs to have exactly the distance of 1
                }

            }

            if (Input.GetMouseButtonUp(1))
            {
                canAccelerate = false;
                Shot();
            }

            if (endTheAttack)       //ending it
            {
                if (Vector2.Distance(transform.position, rotationPivot.transform.position) >= 0.5f) transform.position = Vector2.MoveTowards(transform.position, rotationPivot.transform.position, 1 * 100 * Time.deltaTime);        //going back
                else Destroy(gameObject);
            }
            
            
        }

        void Acceleration()
        {
            if (moveSpeed <= 10) moveSpeed += Time.deltaTime;
            if (rotationSpeed <= 2660) rotationSpeed += Time.deltaTime * 1500;           
        }

        void Shot()
        {
            if (!addForceOnce)
            {
                shotSource.loop = false;
                shotSource.clip = shootClip;
                shotSource.Play();
                StartCoroutine(FindObjectOfType<CameraFollowing>().Shake(0.05f, 0.2f));         //starting screenShake

                mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                Invoke("GoBack", 0.5f);
                body2D.velocity = new Vector3(0, 0, 0);
                body2D.AddForce((Camera.main.ScreenToWorldPoint(mousePos) - transform.position) * 600 * Time.deltaTime, ForceMode2D.Impulse);
                //body2D.AddForce((bullPo.transform.position - rotationPivot.transform.position) * 5600 * Time.deltaTime, ForceMode2D.Impulse);         //Bullpo works currently worse than MousePos
                addForceOnce = true;
            }
        }


        void GoBack()
        {
            body2D.velocity = new Vector3(0, 0, 0);
            endTheAttack = true;         
        }
    }

}
