﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class hostileBullet : MonoBehaviour
    {
        Rigidbody2D body2D;


        [SerializeField]
        float speed = 1;

        void Start()
        {
            body2D = GetComponent<Rigidbody2D>();
        }


        void Update()
        {
            body2D.AddForce(transform.right * speed * Time.deltaTime * 80, ForceMode2D.Impulse);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Destroy(gameObject);
        }
    }
}

