﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class QuestNPC : MonoBehaviour
    {
        [SerializeField]
        scriptableItem[] thePossibleWantedItem;
        scriptableItem theWantedItem;

        scriptableItem[] wantedItems;
        ItemContainer[] container;          //all containers have to be set into trade mode.

        HonestCardManager cardManager;

        [SerializeField]
        GameObject textIndicator;

        TextMeshProUGUI text;

        bool hasItem;
        bool canInteract;
        bool gaveItem;          //needed to "seperate" strings


        private void Start()
        {
            theWantedItem = thePossibleWantedItem[Random.Range(0, thePossibleWantedItem.Length)];
            container = GameObject.Find("Inventory").transform.GetComponentsInChildren<ItemContainer>();
            cardManager = FindObjectOfType<HonestCardManager>();
            text = textIndicator.GetComponent<TextMeshProUGUI>();
        }


        private void Update()
        {
            if (canInteract) if (Input.GetKeyDown("e")) giveItem();
        }

        public void giveItem()
        {
            if (!hasItem)
            {
                for (int i = 0; i < container.Length; i++)
                {
                    if(container[i].heldItem == theWantedItem && !hasItem)
                    {
                        container[i].destroyItem();
                        AddGoodCard();
                    }

                    if(!gaveItem) text.text = "But... you dont have the " + "<#001aff>" + theWantedItem.name + "</color>" + " :(";            //this is only reached in code if the player doesnt have the requested Item.
                }
            }
           
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                textIndicator.SetActive(true);
                if (!hasItem) text.text = "You know.... If I could just enjoy the " + "<#001aff>" + theWantedItem.name + "</color>" + " I would shine with happiness";          //giving Color through rich-tags
                else text.text = "YES! I got it! Thank you so much!:)";
                canInteract = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                textIndicator.SetActive(false);
                canInteract = false;
            }
        }

        void AddGoodCard()
        {
            FindObjectOfType<HonestCardManager>().numberOfGoodDeedCars++;           //adding respect and good deed cards
            FindObjectOfType<HonestCardManager>().RefreshUI();
            StaticValues.respect += 10;

            hasItem = true;
            gaveItem = true;
            text.text = "YES! I got it! Thank you so much!:)";
        }

    }

}
