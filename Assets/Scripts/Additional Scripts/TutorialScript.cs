﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class TutorialScript : MonoBehaviour
    {

        void Start()
        {
            GameObject.Find("LoadingScreen").GetComponent<Animator>().SetTrigger("fadeOut");
            PlayerPrefs.SetInt("hasDoneTutorial", 1);
        }

    }

}
