﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class InstnatiateOnStart : MonoBehaviour
    {
        [SerializeField]
        GameObject[] toInstantiate;

        [SerializeField]
        Transform instantiatePoint;

        [SerializeField]
        bool useTransformRotation, destroyOnEnd, spawnOneRandom, useThisAlsoOnEnable;

        [SerializeField]
        float delay;


        void Start()
        {
            if (instantiatePoint == null) instantiatePoint = transform;
            Invoke("Spawn", delay);           
        }

        private void OnEnable()
        {
            if (useThisAlsoOnEnable)
            {
                if (instantiatePoint == null) instantiatePoint = transform;
                Invoke("Spawn", delay);
            }
        }

        void Spawn()
        {
            if (spawnOneRandom)
            {
                int n = Random.Range(0, toInstantiate.Length);

                if (useTransformRotation) Instantiate(toInstantiate[n], instantiatePoint.position, transform.rotation);
                else Instantiate(toInstantiate[n], instantiatePoint.position, Quaternion.identity);

            }
            else
            {
                for (int i = 0; i < toInstantiate.Length; i++)
                {
                    if (useTransformRotation) Instantiate(toInstantiate[i], instantiatePoint.position, transform.rotation);
                    else Instantiate(toInstantiate[i], instantiatePoint.position, Quaternion.identity);
                }
            }

            if (destroyOnEnd) Destroy(gameObject);
        }
    }

}
