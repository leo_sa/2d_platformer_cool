﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Edgemode_Activator : MonoBehaviour
    {
        PlayerHealth pHealth;

        [SerializeField]
        GameObject lightning, soundEffect, demonVoice, extraExplosion;
        GameObject player, hair;
        GameObject originalPlayerVoice;

        [SerializeField]
        RuntimeAnimatorController edgeController;
        RuntimeAnimatorController originalController;

        Animator animatorPlayer;

        AudioSource musicSource;

        float originalVolume;


        void Start()
        {
            musicSource = GameObject.Find("Music").GetComponent<AudioSource>();
            GetComponent<SpriteRenderer>().enabled = false;
            player = GameObject.Find("Player");
            hair = GameObject.Find("Hair");
            animatorPlayer = player.GetComponent<Animator>();
            pHealth = FindObjectOfType<PlayerHealth>();
            originalPlayerVoice = pHealth.HurtEffect;
            StartCoroutine(EdgeMode());
        }

        IEnumerator EdgeMode()
        {
            DontDestroyOnLoad(gameObject);          //When destroyed onScene, it could end in the Player getting stuck in the Edgemode
            SpawnIn();

            yield return new WaitForSeconds(10);

            SpawnOut();
        }

        void SpawnIn()
        {
            pHealth.canGetHurt = false;         //makes Player invincible

            originalVolume = musicSource.volume;
            musicSource.volume = 0;

            Instantiate(lightning, new Vector2(player.transform.position.x, player.transform.position.y + 10), transform.rotation);         //placing lightning 10 Units above Player
            Instantiate(lightning, new Vector2(player.transform.position.x + 1, player.transform.position.y + 10), transform.rotation);         //placing lightning(2)
            Instantiate(soundEffect, transform.position, transform.rotation);         //placing SFX
            Instantiate(extraExplosion, transform.position, transform.rotation);         //placing Explosion
            hair.SetActive(false);
            originalController = animatorPlayer.runtimeAnimatorController;
            animatorPlayer.runtimeAnimatorController = edgeController;

            pHealth.HurtEffect = demonVoice;
        }

        void SpawnOut()
        {
            pHealth.canGetHurt = true;

            musicSource.volume = originalVolume;

            pHealth.HurtEffect = originalPlayerVoice;

            Instantiate(lightning, new Vector2(player.transform.position.x, player.transform.position.y + 10), transform.rotation);
            Instantiate(lightning, new Vector2(player.transform.position.x + 1, player.transform.position.y + 10), transform.rotation);
            hair.SetActive(true);
            animatorPlayer.runtimeAnimatorController = originalController;
            Destroy(gameObject);
        }
    }
}

