﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class damageText : MonoBehaviour
    {
        Rigidbody2D rb2d;

        TextMeshPro text;

        [SerializeField, Range(0, 0.2f)]
        float fadeOutSpeed;                         //What LERP-rate will the Text fade out?

        float fadeOut;                  //used for the Alpha value of the VertexColor in the instance of the TMPRO_Text;

        Color originalColor;            //used to reference the Original VertexColor;

        private void Start()
        {
            text = GetComponent<TextMeshPro>();
            originalColor = text.color;
            rb2d = GetComponent<Rigidbody2D>();
            rb2d.AddForce(Vector2.right * Random.Range(-1, 1), ForceMode2D.Impulse);
            fadeOut = originalColor.a;              //For better modularity, fadeOut will start at the original Alpha-Value.
        }

        void Update()
        {
            transform.Translate(Vector3.up * 0.02f);
            text.color = new Color(originalColor.r, originalColor.g, originalColor.b, fadeOut);
            fadeOut = Mathf.Lerp(fadeOut, 0, fadeOutSpeed);

            if (fadeOut <= 0.05f) Destroy(gameObject);              //Destroying when not visible anymore.
        }
    }
}

