﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class audioSilencer : MonoBehaviour
    {
        [SerializeField]
        AudioSource toSilence;
        AudioSource thisAudio;

        bool lerpTo, lerpBack;

        float originalVolume, thisOriginalVolume;


        private void Start()
        {
            thisAudio = GetComponent<AudioSource>();
            originalVolume = toSilence.volume;
            thisOriginalVolume = thisAudio.volume;

            thisAudio.volume = 0.1f;
        }

        private void Update()
        {
            if (lerpTo)
            {
                thisAudio.volume = Mathf.Lerp(thisAudio.volume, thisOriginalVolume, 0.2f);
                if (thisAudio.volume - thisOriginalVolume / 2 <= 0.1f) thisAudio.volume = thisOriginalVolume;


                toSilence.volume = Mathf.Lerp(toSilence.volume, originalVolume / 2, 0.2f);
                if (toSilence.volume - originalVolume / 2 <= 0.1f) toSilence.volume = originalVolume / 2;
            }
            else if (lerpBack)
            {
                toSilence.volume = Mathf.Lerp(toSilence.volume, originalVolume, 0.2f);
                if (toSilence.volume - originalVolume <= 0.1f) toSilence.volume = originalVolume;


                thisAudio.volume = Mathf.Lerp(thisAudio.volume, 0.1f, 0.2f);
                if (toSilence.volume - 0.1f <= 0.1f) toSilence.volume = 0.1f;
            }
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                lerpTo = true;
                lerpBack = false;
            }
        }


        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                lerpTo = false;
                lerpBack = true;
            }
        }



    }

}
