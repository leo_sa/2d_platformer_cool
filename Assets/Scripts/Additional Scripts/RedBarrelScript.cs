﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class RedBarrelScript : MonoBehaviour2
    {
        AudioSource audioSource;
        [SerializeField]
        AudioClip explosionClip;

        [SerializeField]
        GameObject explosion;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Bullet>() || collision.gameObject.GetComponent<meeleeHit>() || collision.gameObject.GetComponent<DamageEnemy>() || collision.gameObject.GetComponent<hostileBullet>())
            {
                MyPlay(audioSource, explosionClip);
                FindObjectOfType<GameManager>().SlowMotion(0.05f, 0.02f);
                Invoke("Explode", 0.2f);
            }            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.GetComponent<Bullet>() || collision.gameObject.GetComponent<meeleeHit>() || collision.gameObject.GetComponent<DamageEnemy>() || collision.gameObject.GetComponent<hostileBullet>())
            {
                MyPlay(audioSource, explosionClip);
                FindObjectOfType<GameManager>().SlowMotion(0.05f, 0.02f);
                Invoke("Explode", 0.2f);
            }
        }

        void Explode()
        {
            Instantiate(explosion, transform.position, transform.rotation);

            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Rigidbody2D>().isKinematic = true;         //disabling everything except audiosource
            GetComponent<Collider2D>().enabled = false;

            Destroy(gameObject, 12);
        }
    }

}
