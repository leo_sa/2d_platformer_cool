﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class screenShake : MonoBehaviour
    {
        [SerializeField]
        public float lenght, magnitude;

        void Start()
        {
            StartCoroutine(Camera.main.GetComponent<CameraFollowing>().Shake(lenght, magnitude));
        }


    }

}
