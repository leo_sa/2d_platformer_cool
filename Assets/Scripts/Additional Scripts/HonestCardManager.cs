﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;          //for onSceneLoad
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class HonestCardManager : MonoBehaviour2
    {
        TextMeshProUGUI goodNumber, badNumber;

        [HideInInspector]
        public int numberOfGoodDeedCars, numberOfBadDeedCards;          //referenced in Honest Manager

        //[SerializeField]
        public List<int> badDeedsHeldIn, potentialDecreases;

        TownManager townManager;                //for decrease Methods

        HonestManager honestManager;              //for socialStatusReference

        void Start()
        {
            numberOfGoodDeedCars++;
            goodNumber = GameObject.Find("GoodNumber").GetComponent<TextMeshProUGUI>();
            badNumber = GameObject.Find("BadNumber").GetComponent<TextMeshProUGUI>();

            goodNumber.text = numberOfGoodDeedCars + "x";
            badNumber.text = numberOfBadDeedCards + "x";

            if (FindObjectOfType<TownManager>()) townManager = FindObjectOfType<TownManager>();
            honestManager = FindObjectOfType<HonestManager>();
            
        }
        

        public void PlayGoodDeed()
        {
            if(numberOfGoodDeedCars > 0)
            {
                Log("PlayGoodCard");
                try
                {
                    townManager.IncreaseRespect(15 * honestManager.socialStatus);
                }
                catch
                {
                    if (FindObjectOfType<TownManager>()) townManager = FindObjectOfType<TownManager>();
                    townManager.IncreaseRespect(15 * honestManager.socialStatus);
                }
                //if the try is unexecutable, the townManagerReference is missing. Either because it was never found or was destroyed onSceneLoad.

                numberOfGoodDeedCars--;
                RefreshUI();
            }
           
        }

        public void PlayBadDeed()
        {
            if (numberOfBadDeedCards > 0)
            {
                try
                {
                    townManager.DecreaseRespect((5 * badDeedsHeldIn[numberOfBadDeedCards - 1 + 1] + potentialDecreases[numberOfBadDeedCards - 1]) * honestManager.socialStatus * numberOfBadDeedCards);
                }
                catch
                {
                    if (FindObjectOfType<TownManager>()) townManager = FindObjectOfType<TownManager>();
                    townManager.DecreaseRespect((5 * (badDeedsHeldIn[numberOfBadDeedCards - 1] + 1) + potentialDecreases[numberOfBadDeedCards - 1]) * honestManager.socialStatus * numberOfBadDeedCards);
                }                         

                badDeedsHeldIn.RemoveAt(numberOfBadDeedCards - 1);
                potentialDecreases.RemoveAt(numberOfBadDeedCards - 1);

                numberOfBadDeedCards--;
                RefreshUI();
            }      
        }

        public void RefreshUI()
        {
            honestManager.respecText.text = "Rep." + StaticValues.respect;
            goodNumber.text = numberOfGoodDeedCars + "x";
            badNumber.text = numberOfBadDeedCards + "x";
        }
    }

}
