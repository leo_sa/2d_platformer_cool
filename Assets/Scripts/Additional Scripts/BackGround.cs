﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class BackGround : MonoBehaviour
    {
        [SerializeField]                            
        bool folowCamera;

        Transform cam;

         void Start()
         {
            cam = Camera.main.transform;
         }

   
        void Update()
        {
            if (folowCamera) transform.position = new Vector3(cam.position.x, cam.position.y, 0);       //0 has to be set manually, because camera.transform.z = -10
        }
    }
}
