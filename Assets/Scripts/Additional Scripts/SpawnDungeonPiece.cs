﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{

    public class SpawnDungeonPiece : MonoBehaviour
    {
        GameObject[] nextPart;           //saving DungeonPart;
        public bool end, mid, start;                        //used to send signal to dungeon_Manager  

        public bool newLevelGen, up;

        void Start()
        {
            if (newLevelGen)
            {
                if (start)
                {
                    if(up) nextPart = FindObjectOfType<DungeonManager>().midsUp;
                    else nextPart = FindObjectOfType<DungeonManager>().midsDown;
                }
                else if (mid)
                {
                    if (up) nextPart = FindObjectOfType<DungeonManager>().startsUp;
                    else nextPart = FindObjectOfType<DungeonManager>().endsDown;
                }
                else if (end)
                {
                    if (up) nextPart = FindObjectOfType<DungeonManager>().midsUp;
                    else nextPart = FindObjectOfType<DungeonManager>().endsUp;
                }
            }
            else
            {
                if (start) nextPart = FindObjectOfType<DungeonManager>().mids;
                else if (mid) nextPart = FindObjectOfType<DungeonManager>().ends;
                else if (end) nextPart = null;
            }

            if (nextPart != null) Instantiate(nextPart[Random.Range(0, nextPart.Length)], transform.position, Quaternion.identity);
            else
            {
                FindObjectOfType<DungeonManager>().canChange = true;
                end = false;
            }
        }


    }
}
