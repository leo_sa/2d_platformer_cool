﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class RayProjectile : MonoBehaviour
    {
        [SerializeField]
        LayerMask lay;

        RaycastHit2D hit;

        [SerializeField]
        GameObject actualRay;

        [SerializeField]
        int length;

        void Start()
        {      
            hit = Physics2D.Raycast(transform.position, transform.right, length, lay);
            if(hit.collider != null) Instantiate(actualRay, hit.point, Quaternion.identity);
            Destroy(gameObject);
        }
    }

}
