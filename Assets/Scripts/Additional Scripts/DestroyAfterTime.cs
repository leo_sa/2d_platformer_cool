﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class DestroyAfterTime : MonoBehaviour
    {
        [SerializeField]
        float delay;

        void Start()
        {
            Destroy(gameObject, delay);
        }


    }
}

