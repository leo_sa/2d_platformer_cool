﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CoATwoPR
{
    public class TraderItemContainer : MonoBehaviour
    {
        Sprite itemSprite;
        Sprite originalSprite;

        Image image;

        TextMeshProUGUI textName, textWorth, edibleText;

        [SerializeField]
        scriptableItem[] possibleItems;

        [SerializeField]
        GameObject[] possibleBuyables;

        GameManager manager;            //for money reference

        PlayerHealth health;

        scriptableItem heldItem;
        GameObject heldObject;

        ItemContainer[] container;

        [SerializeField]
        int[] manualWoth;
        int weaponIndex;                //used to determine worth for every object to instantiate


        [SerializeField]
        string[] manualNames;
        string manualName;

        float richFactor;               //multiplay the value for balancing.       

        float worth;

        [HideInInspector]
        public bool holdsItem;
        bool canHealPlayer;

        string name_;

        [SerializeField]
        AudioClip buy, missingMoney;
        AudioSource audioSource;

        private void Start()
        {
            richFactor = (1-StaticValues.respect / 300);
            if (richFactor < 0) richFactor = 0;

            container = GameObject.Find("Inventory").transform.GetComponentsInChildren<ItemContainer>();

            image = transform.GetChild(0).GetChild(1).GetComponent<Image>();
            originalSprite = image.sprite;
            textName = transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
            textWorth = transform.GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>();
            edibleText = transform.GetChild(0).GetChild(3).GetComponent<TextMeshProUGUI>();
            audioSource = GetComponent<AudioSource>();

            health = FindObjectOfType<PlayerHealth>();
            manager = FindObjectOfType<GameManager>();

            if(possibleItems.Length != 0)heldItem = possibleItems[Random.Range(0, possibleItems.Length)];                //one Random Item please!

            if(possibleBuyables.Length != 0)
            {
                weaponIndex = Random.Range(0, possibleBuyables.Length);
                manualName = manualNames[weaponIndex];
                heldObject = possibleBuyables[weaponIndex];                //one Random Weapon please!
            }

            Refresh();
        }

        public void destroyItem()
        {
            if (worth != 0)          //could be (nearly) any variable. Just to check if an Item is being held.
            {
                image.sprite = originalSprite;
                worth = 0;
                textName.text = "Bought!";
                textWorth.text = "Its yours!";
                edibleText.text = "YOURS";
            }         
        }

        public void buyItem()
        {
            if (holdsItem)
            {
                if(possibleItems.Length != 0)           //if selling normal items
                {
                    if (worth <= manager.money)
                    {
                        for (int i = 0; i < container.Length; i++)
                        {
                            if (!container[i].holdsItem)
                            {
                                if (holdsItem) container[i].addItem(heldItem);
                                holdsItem = false;
                            }
                        }

                        if (!holdsItem)         //item succesfully bought
                        {
                            manager.money -= (int)worth;
                            manager.RefreshCoinText();

                            audioSource.clip = buy;
                            audioSource.pitch = Random.Range(0.95f, 1.05f);
                            audioSource.Play();

                            destroyItem();
                        }
                        else UnableToBuy();

                    }
                    else UnableToBuy();
                }
                else
                {
                    if (worth <= manager.money)
                    {

                        Instantiate(heldObject, transform.parent.transform.parent.transform.parent.transform.GetChild(1).transform.position, transform.rotation);
                     
                            manager.money -= (int)worth;
                            manager.RefreshCoinText();

                            audioSource.clip = buy;
                            audioSource.pitch = Random.Range(0.95f, 1.05f);
                            audioSource.Play();

                            destroyItem();                      
                    }
                    else UnableToBuy();
                }
               
            }           
            
        }

        void UnableToBuy()
        {                                                 
                audioSource.clip = missingMoney;
                audioSource.pitch = Random.Range(0.95f, 1.05f);
               audioSource.Play();           
        }

        void Refresh()
        {
            if(possibleItems.Length != 0)
            {
                name_ = heldItem.itemName;
                worth = (int)(heldItem.worth * richFactor);
                itemSprite = heldItem.sprite;
                image.sprite = itemSprite;
                textName.text = heldItem.itemName;
                textWorth.text = worth.ToString();
                canHealPlayer = heldItem.canHeal;
                if (canHealPlayer) edibleText.text = "tasty";
                else edibleText.text = "disgusting";

                holdsItem = true;
            }
            else
            {
                name_ = manualName;
                worth = manualWoth[weaponIndex] * richFactor;      
                itemSprite = heldObject.GetComponent<SpriteRenderer>().sprite;
                image.sprite = itemSprite;
                textName.text = name_;
                textWorth.text = worth.ToString();
                canHealPlayer = false;
                if (canHealPlayer) edibleText.text = "tasty";
                else edibleText.text = "BANG";

                holdsItem = true;
            }
           
        }

        public void refreshPrice()          //activated in NPC
        {
            if (holdsItem)          //not refreshed if already bought
            {
                richFactor = (1 - StaticValues.respect / 300);
                if (richFactor < 0) richFactor = 0;
                try
                {
                    worth = (int)(heldItem.worth * richFactor);             //this will be on the Item trade, the Catch will be on the Weapon-Trader
                }
                catch
                {
                    worth = manualWoth[weaponIndex] * richFactor;
                }
                textWorth.text = worth.ToString();
            }
           
        }


    }
}


