﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class meeleeWeapon : MonoBehaviour2
    {
        Gun gun;
        PlayerMovement move;            //for recoil
        PlayerHealth health;        //hide gun sprite when dead
        GameManager manager;            //fro Ammo

        [SerializeField]
        SpriteRenderer armsSpriteRenderer;
        SpriteRenderer spriteRenderer;

        AudioSource audioSource;
        AudioClip useClip, spanClip;

        Animator animator;
       
        GameObject meeleeHit_;
        GameObject lokalSpawn;              //for continual Attack, has to bes stored as variable so it can be destroyed when get mouse button up

        public scriptableMeeleeWeapon weapon;

        int ammoUsage;

        float windUpLength;
        float hitStayLength;
        float unShootDelay;
        float x, y;         //recoil
        float accuracy;     //for bows
        float accuracySpeed;        //how fast will it be?

        [HideInInspector]
        public bool isAttacking;        //referenced in Gun for sprites enabling
        bool continualInput, spawnOnClick, useContinualAnimation, dontDestroyOnUp;        
        
        

        void Start()
        {
            gun = FindObjectOfType<Gun>();
            move = FindObjectOfType<PlayerMovement>();
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.enabled = false;                     //should only be active when attacking
            health = FindObjectOfType<PlayerHealth>();
            audioSource = GetComponent<AudioSource>();
            manager = FindObjectOfType<GameManager>();

            refreshStats();
        }


        public void refreshStats()
        {
            windUpLength = weapon.windUpLength;
            useClip = weapon.useClip;
            hitStayLength = weapon.hitStayLength;
            meeleeHit_ = weapon.meeleeHit;
            animator.runtimeAnimatorController = weapon.weaponAnimation;
            spriteRenderer.sprite = weapon.worldSprite;
            x = weapon.horizontalForce;
            y = weapon.verticalForce;
            unShootDelay = weapon.unAttackDelay;
            accuracySpeed = weapon.accuracySpeed;
            ammoUsage = weapon.ammoUsage;
            spanClip = weapon.spanClip;
            dontDestroyOnUp = weapon.dontDestroyOnUp;

            continualInput = weapon.useContinualInput;
            spawnOnClick = weapon.onClickSpawn;
            useContinualAnimation = weapon.useAnimationContinual;
        }


        void Update()
        {
            if (!StaticValues.isDead && !gun.safe)       //if player is still alive and not in social-mode
            {
                if (Input.GetMouseButtonDown(1))
                {
                    if (gun.muzzleFlash.activeInHierarchy) gun.muzzleFlash.SetActive(false);         //no muzzleflash
                    if (gun.extraAudioSource.isPlaying) gun.extraAudioSource.Stop();        //no hissing if using meelee
                }

                if (!continualInput)
                {
                    if (Input.GetMouseButtonDown(1) && !isAttacking)
                    {
                        if (move.canMove && !gun.safe && gun.canbeUsed)               //canBeUsed is false, when game is paused etc. This also applies for this meele weapon. This is the shorter variation of adding another bool and setting it to false in other scripts.
                        {
                            animator.SetTrigger("attack");

                            Invoke("DelayAttack", windUpLength);
                        }
                    }
                }
                else
                {
                    if ((manager.ammoAvailable > 0 && ammoUsage > 0 && !gun.isReloading) || ammoUsage == 0)
                    {
                        if (Input.GetMouseButtonDown(1))
                        {
                            if (spanClip != null) MyPlay(audioSource, spanClip);
                            //gun.shot.Stop();                //stop the shooting sound!

                            accuracy = 30;       //accuracy reset Hardcoded to X*2 degrees

                            if (spawnOnClick) SpawnProjectile();            //spawning the shot

                            isAttacking = true;
                            spriteRenderer.enabled = true;
                            gun.canbeUsed = false;
                            gun.spriteRenderer.enabled = false;
                            gun.animator.SetBool("isShooting", false);

                            armsSpriteRenderer.enabled = false;

                            if (useContinualAnimation)
                            {
                                if (gun.spriteRenderer.flipY) spriteRenderer.flipY = true;
                                else spriteRenderer.flipY = false;
                                animator.SetTrigger("spann");
                                animator.ResetTrigger("let");
                            }
                        }
                        else if (Input.GetMouseButton(1))
                        {
                            if (accuracy > 0) accuracy -= Time.deltaTime * accuracySpeed;
                            else if (accuracy < 0) accuracy = 0;
                        }
                        else if (Input.GetMouseButtonUp(1))
                        {
                            if (!spawnOnClick) SpawnProjectile();        //spawning the shot

                            else if(!dontDestroyOnUp) Destroy(lokalSpawn);

                            if (useContinualAnimation)
                            {
                                if (gun.spriteRenderer.flipY) spriteRenderer.flipY = true;
                                else spriteRenderer.flipY = false;
                                animator.SetTrigger("let");
                                animator.ResetTrigger("spann");
                            }
                            Invoke("UnAttack", unShootDelay);
                        }
                        manager.RefreshAmmoText();

                    }

                }
            }          
            
        }

        void SpawnProjectile()
        {
            MyPlay(audioSource, useClip);
            manager.ammoAvailable -= ammoUsage;
            lokalSpawn = Instantiate(meeleeHit_, transform.position, Quaternion.Euler(0, 0, transform.eulerAngles.z + Random.Range(-accuracy, accuracy)));
        }


        void DelayAttack()
        {
            isAttacking = true;
            spriteRenderer.enabled = true;
            gun.canbeUsed = false;
            gun.spriteRenderer.enabled = false;
            armsSpriteRenderer.enabled = false;




            StartCoroutine(Attack());
        }


        IEnumerator Attack()
        {
            if(move.spriteRenderer.flipX) move.extraForce += x;
           else move.extraForce -= x;

            MyPlay(audioSource, useClip);

            move.rb2d.AddForce(Vector2.up * y, ForceMode2D.Impulse); 
            Instantiate(meeleeHit_, transform.position, transform.rotation);

            yield return new WaitForSeconds(hitStayLength);

            UnAttack();

        }   

        void UnAttack()
        {
            isAttacking = false;
            spriteRenderer.enabled = false;
            gun.canbeUsed = true;
            if (health.health >= 0)
            {
                armsSpriteRenderer.enabled = true;
            }
        }
       


    }

}
