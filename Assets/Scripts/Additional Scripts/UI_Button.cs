﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class UI_Button : MonoBehaviour              //for loading scenes
    {
        [SerializeField]
        string levelName;

        [SerializeField]
        LoadSceneMode Loadscene;

        [SerializeField]
        bool backToStartingRoom;                            //When going back to startingRoom, every Gameobject, even DontDestroyOnLoad-onces have to be destroyed         

        GameObject[] gameObjects;               //this will be done via an Array.

        public void load()
        {
            if (backToStartingRoom)
            {
                StaticValues.resetValues();
                gameObjects = FindObjectsOfType<GameObject>();

                for (int i = 0; i < gameObjects.Length; i++)
                {
                    Destroy(gameObjects[i]);
                }
            }


            SceneManager.LoadScene(levelName, Loadscene);
        }

        public void exit()
        {
            Application.Quit();
        }
        
    }

}
