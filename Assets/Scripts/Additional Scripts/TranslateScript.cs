﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class TranslateScript : MonoBehaviour
    {
        [SerializeField]
        float speedX;

        void Update()
        {
            transform.Translate(transform.right * speedX);
        }
    }

}
