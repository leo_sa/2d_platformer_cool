﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class DrawLineFromTo : MonoBehaviour
    {
        LineRenderer line;

        [SerializeField]
        Transform setTo;

        [SerializeField]
        bool usePlayerAsPoint;

        void Start()
        {          
            line = GetComponent<LineRenderer>();
            if (usePlayerAsPoint) setTo = GameObject.Find("Player").transform;
        }


        void Update()
        {     
            line.SetPosition(0, transform.position);
            line.SetPosition(1, setTo.transform.position);
        }
    }

}
