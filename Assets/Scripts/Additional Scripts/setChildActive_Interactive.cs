﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class setChildActive_Interactive : MonoBehaviour
    {
        [SerializeField]
        GameObject toSet;

        void Start()
        {
            if (toSet == null) toSet = transform.GetChild(0).gameObject;         //leaving it modular
            toSet.SetActive(false);
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player") toSet.SetActive(true);
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player") toSet.SetActive(false);
        }
    }

}
