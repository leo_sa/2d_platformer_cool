﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class townSetter : MonoBehaviour
    {
        [SerializeField]
        GameObject[] towns_T1, towns_T2, towns_T3, towns_T4;

        int tier;

        void Start()
        {
            tier = FindObjectOfType<GameManager>().tier;
            SpawnTowns();
            //Instantiate(towns[Random.Range(0, towns.Length)], transform.position, Quaternion.identity);
            GameObject.Find("LoadingScreen").GetComponent<Animator>().SetTrigger("fadeOut");
            Destroy(gameObject);
        }

        void SpawnTowns()
        {
            if (tier == 2) Instantiate(towns_T1[Random.Range(0, towns_T1.Length)], transform.position, Quaternion.identity);            //"offset" of one, because tier-increase happens before the town is spawned
            else if (tier == 3) Instantiate(towns_T2[Random.Range(0, towns_T2.Length)], transform.position, Quaternion.identity);
            else if (tier == 4) Instantiate(towns_T3[Random.Range(0, towns_T3.Length)], transform.position, Quaternion.identity);
            else if (tier >= 5) Instantiate(towns_T4[Random.Range(0, towns_T4.Length)], transform.position, Quaternion.identity);
        }

       
    }

}
