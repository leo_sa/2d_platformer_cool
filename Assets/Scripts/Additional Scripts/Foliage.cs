﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Foliage : MonoBehaviour
    {
        Animator anim;

        void Start()
        {
            anim = GetComponent<Animator>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(anim != null) anim.SetTrigger("activate");
        }
    }

}
