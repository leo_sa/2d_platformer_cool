﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Viber : MonoBehaviour
    {
        [SerializeField]
        GameObject present;

        bool spawnPresent;

        Transform player;

        [SerializeField]
        float minAttackWaitTime = 2, maxAttackWaitTime = 8;

        void Start()
        {
            player = GameObject.Find("Player").transform;
            StartCoroutine(Attack());
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 3, ForceMode2D.Impulse);
        }


        void Update()
        {
            if (Vector2.Distance(transform.position, player.position) < 10)
            {
                if (spawnPresent) StartCoroutine(Attack());
            }
        }

        IEnumerator Attack()
        {
            spawnPresent = false;

            yield return new WaitForSeconds(Random.Range(minAttackWaitTime, maxAttackWaitTime));

            Instantiate(present, transform.position, Quaternion.identity);
            spawnPresent = true;
        }
    }
}

