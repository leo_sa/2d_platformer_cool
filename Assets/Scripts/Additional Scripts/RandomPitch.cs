﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class RandomPitch : MonoBehaviour
    {       
        void Start()
        {
            GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
        }     
    }

}
