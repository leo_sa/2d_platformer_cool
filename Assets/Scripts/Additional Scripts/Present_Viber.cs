﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Present_Viber : MonoBehaviour
    {
        [SerializeField]
        GameObject attack;

        void Attacker()
        {
            Instantiate(attack, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            Destroy(gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Invoke("Attacker", Random.Range(0, 5));
        }
    }

}
