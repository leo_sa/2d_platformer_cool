﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Coin : MonoBehaviour
    {       
        GameObject player;

        GameManager gameManager;

        Rigidbody2D rb2d;

        [SerializeField]
        bool onlyPick;

        bool canFollow;             //needed for delay, so coins burst out and THEN follow the player


        void Start()
        {
            if (!onlyPick) Invoke("EnableFollow", Random.Range(1, 2));
            player = GameObject.Find("Player");
            gameManager = FindObjectOfType<GameManager>();
            rb2d = GetComponent<Rigidbody2D>();
            transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            rb2d.AddForce(transform.up * 4, ForceMode2D.Impulse);
        }


        void Update()
        {        
                if (canFollow)
                {
                    rb2d.gravityScale = 0;
                    transform.position = Vector2.Lerp(transform.position, player.transform.position, 0.4f);             //for some odd reason the number HAS to be above 0.3f;
                    rb2d.MovePosition(transform.position + player.transform.position);
                    if (Vector2.Distance(transform.position, player.transform.position) <= 0.5f) PickCoin();
                }         
        }

        void EnableFollow()
        {
            canFollow = true;
        }

        void PickCoin()
        {           
            gameManager.money++;
            gameManager.RefreshCoinText();
            Destroy(gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.name.Equals("Player")) PickCoin();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name.Equals("Player")) PickCoin();
        }
    }

}
