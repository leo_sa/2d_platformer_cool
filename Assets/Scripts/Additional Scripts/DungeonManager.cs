﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;        //for loadingscreen text

namespace CoATwoPR
{
    public class DungeonManager : MonoBehaviour
    {
        TextMeshProUGUI text;

        Animator loadingscreenAnimator;         //reference to loadingscreen, so the fadeOut effect can be triggered

        [SerializeField]
        AudioClip[] tierMusic;
        AudioSource musicSource;

        [SerializeField]
        RuntimeAnimatorController[] Foliage_t1;             //all Tier foliage. Also has to be theme-parralel
        [SerializeField]
        RuntimeAnimatorController[] Foliage_t2;
        [SerializeField]
        RuntimeAnimatorController[] Foliage_t3;
        [SerializeField]
        RuntimeAnimatorController[] Foliage_4;
        
        public GameObject[] start, mids, ends;               //Dungeon Prefabs

        public GameObject[] startsUp, startsDown, midsUp, midsDown, endsUp, endsDown;
        
        public GameObject dungeonExit;             //for instanciating the Dungeon Exit

        [SerializeField]
        GameObject newLevelGenerator;

        List<GameObject> horizontalTiles;

        List<GameObject> verticalTiles;

        List<GameObject> groundTiles;

        List<GameObject> fullTiles;

        List<GameObject> extraSpawner;

        List<GameObject> spawner;

        Transform bossSpawner;                              //only one boss will be spawned at ONE position
        
        public List<GameObject> enemies;                //A list of all enemies, to determine when the level will be finished. Not hidden in inspector for debugging reasons

        GameObject[] tiles;         //used for every Tile to later set to the Tile-Theme

        GameObject player;

        [HideInInspector]
        public bool canChange;              //Can the Theme be set?

        bool isLoading = true;              //is the Level currently loading? Used to determine player position, loadingscreen etc.

        bool useOldGeneration;

        int tier = 1;                       //The current difficulty

        [HideInInspector]
        public int level;                  //The current Level(Levels Passed)

        int theme_t1, theme_t2, theme_t3, theme_t4;         //The current set theme

        [SerializeField]
        Sprite[] HorizontalTiles_T1, VerticalTiles_T1, GroundTiles_T1, fullTiles_T1;              //All Tiles for Tier1

        [SerializeField]
        Sprite[] HorizontalTiles_T2, VerticalTiles_T2, GroundTiles_T2, fullTiles_T2;              //All Tiles for Tier2

        [SerializeField]
        Sprite[] HorizontalTiles_T3, VerticalTiles_T3, GroundTiles_T3, fullTiles_t3;              //All Tiles for Tier3

        [SerializeField]
        Sprite[] HorizontalTiles_T4, VerticalTiles_T4, GroundTiles_T4, fullTiles_t4;              //All Tiles for Tier4

        [SerializeField]
        GameObject[] toSpawn_T1;                                    //All spawnable enemies, events, chests etc. for Tier_1;

        [SerializeField]
        GameObject[] toSpawn_T2;                                    //All spawnable enemies, events, chests etc. for Tier_2;

        [SerializeField]
        GameObject[] toSpawn_T3;                                    //All spawnable enemies, events, chests etc. for Tier_3;

        [SerializeField]
        GameObject[] toSpawn_T4;                                    //All spawnable enemies, events, chests etc. for Tier_4;

        [SerializeField]
        GameObject[] backGround_T1;                                        //All Backgrounds T1

        [SerializeField]
        GameObject[] backGround_T2;                                        //All Backgrounds T2

        [SerializeField]
        GameObject[] backGround_T3;                                        //All Backgrounds T3

        [SerializeField]
        GameObject[] backGround_T4;                                        //All Backgrounds T4

        [SerializeField]
        GameObject[] extras;                                                //extra levels generation, for example destroyable Tiles

        [SerializeField]
        GameObject[] boss_T1;                                       //all bosses

        [SerializeField]
        GameObject[] boss_T2;

        [SerializeField]
        GameObject[] boss_T3;

        [SerializeField]
        GameObject[] boss_T4;


        void Start()
        {         

            loadingscreenAnimator = GameObject.Find("LoadingScreen").GetComponent<Animator>();

            tier = FindObjectOfType<GameManager>().tier;         

            player = GameObject.Find("Player");

            theme_t1 = Random.Range(0, HorizontalTiles_T1.Length);                      //Amount of HorizontalTiles is alway parralel with VerticalTiles and Themesavaliable. Has to be set in Start, otherwise level would be Completely randomly generated, meaning everything would be stilistically inconsistent.
            theme_t2 = Random.Range(0, HorizontalTiles_T2.Length);
            theme_t3 = Random.Range(0, HorizontalTiles_T3.Length);
            theme_t4 = Random.Range(0, HorizontalTiles_T4.Length);

            level = FindObjectOfType<GameManager>().level;
            text = GameObject.Find("LoadingText").GetComponent<TextMeshProUGUI>();

            musicSource = GameObject.Find("Music").GetComponent<AudioSource>();
            //Debug.Log(tier);


            try
            {
                if (musicSource.clip != tierMusic[tier - 1])
                {
                    musicSource.clip = tierMusic[tier - 1];            //setting Music
                    musicSource.Play();
                }
            }
            catch
            {
                musicSource.clip = null;          //removing Music
                musicSource.Stop();
            }
               
            

            if (useOldGeneration)
            {
                if (tier <= 2) Instantiate(start[Random.Range(0, start.Length)], transform.position, Quaternion.identity);
                else Instantiate(startsDown[Random.Range(0, startsDown.Length)], transform.position, Quaternion.identity);              //after the second Tier new LevelGen will take place
            }
            else
            {               //one of three possible Levengenerations will take Place
                int n = Random.Range(0, 4);

                if (n <= 1) Instantiate(start[Random.Range(0, start.Length)], transform.position, Quaternion.identity);
                else if (n == 2) Instantiate(startsDown[Random.Range(0, startsDown.Length)], transform.position, Quaternion.identity);
                else
                {
                   if (level % 5 != 0) Instantiate(newLevelGenerator, transform.position, Quaternion.identity);         //if boss is spawned, dont use newest levelGeneration, use the first iteration.
                    else Instantiate(start[Random.Range(0, start.Length)], transform.position, Quaternion.identity);
                }

            }
            //player.transform.position = GameObject.Find("PlayerSpawn").transform.position;
        }

        private void Update()
        {
            if (canChange)
            {
                Time.timeScale = 0.05f;          //slow time to simulate pause

                text.text = "Initiating Lists";

                horizontalTiles = new List<GameObject>(GameObject.FindGameObjectsWithTag("HorizontalTile"));
                verticalTiles = new List<GameObject>(GameObject.FindGameObjectsWithTag("VerticalTile"));
                groundTiles = new List<GameObject>(GameObject.FindGameObjectsWithTag("GroundTile"));
                fullTiles = new List<GameObject>(GameObject.FindGameObjectsWithTag("FullTile"));
                spawner = new List<GameObject>(GameObject.FindGameObjectsWithTag("Spawner"));
                extraSpawner = new List<GameObject>(GameObject.FindGameObjectsWithTag("extraSpawner"));

                text.text = "Setting Horizontal-Tiles";

                for (int i = 0; i < horizontalTiles.Count; i++)
                {
                    if (tier == 1)
                    {
                        horizontalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = HorizontalTiles_T1[theme_t1];
                        if (horizontalTiles[i].transform.childCount > 0) if (horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>()) horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController = Foliage_t1[theme_t1];          //if Child has animator, that means it is supposed to have foliage.
                    }
                    else if (tier == 2)
                    {
                        horizontalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = HorizontalTiles_T2[theme_t2];
                        if (horizontalTiles[i].transform.childCount > 0) if (horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>()) horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController = Foliage_t2[theme_t2];
                    }
                    else if (tier == 3)
                    {
                        horizontalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = HorizontalTiles_T3[theme_t3];
                        if (horizontalTiles[i].transform.childCount > 0) if (horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>()) horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController = Foliage_t3[theme_t3];
                    }
                    else if (tier >= 4)
                    {
                        horizontalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = HorizontalTiles_T4[theme_t4];
                        if (horizontalTiles[i].transform.childCount > 0) if (horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>()) horizontalTiles[i].transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController = Foliage_t3[theme_t3];
                    }

                }

                text.text = "Setting Vertical-Tiles";

                for (int i = 0; i < verticalTiles.Count; i++)
                {
                    if (tier == 1) verticalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = VerticalTiles_T1[theme_t1];
                    else if (tier == 2) verticalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = VerticalTiles_T2[theme_t2];
                    else if (tier == 3) verticalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = VerticalTiles_T3[theme_t3];
                    else if (tier >= 4) verticalTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = VerticalTiles_T4[theme_t4];

                }

                text.text = "Setting Ground-Tiles";

                for (int i = 0; i < groundTiles.Count; i++)
                {
                    if (tier == 1) groundTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = GroundTiles_T1[theme_t1];
                    else if (tier == 2) groundTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = GroundTiles_T2[theme_t2];
                    else if (tier == 3) groundTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = GroundTiles_T3[theme_t3];
                    else if (tier >= 4) groundTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = GroundTiles_T4[theme_t4];

                }

                text.text = "Setting Full-Tiles";
                for (int i = 0; i < fullTiles.Count; i++)
                {
                    if (tier == 1) fullTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = fullTiles_T1[theme_t1];
                    else if (tier == 2) fullTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = fullTiles_T1[theme_t2];
                    else if (tier == 3) fullTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = fullTiles_T1[theme_t3];
                    else if (tier >= 4) fullTiles[i].gameObject.GetComponent<SpriteRenderer>().sprite = fullTiles_T1[theme_t4];

                }

                text.text = "Spawning spawnables";

                for (int i = 0; i < spawner.Count; i++)
                {
                    if (tier == 1) Instantiate(toSpawn_T1[Random.Range(0, toSpawn_T1.Length)], spawner[i].gameObject.transform.position, Quaternion.identity);
                    else if (tier == 2) Instantiate(toSpawn_T2[Random.Range(0, toSpawn_T2.Length)], spawner[i].gameObject.transform.position, Quaternion.identity);
                    else if (tier == 3) Instantiate(toSpawn_T3[Random.Range(0, toSpawn_T3.Length)], spawner[i].gameObject.transform.position, Quaternion.identity);
                    else if (tier >= 4) Instantiate(toSpawn_T4[Random.Range(0, toSpawn_T4.Length)], spawner[i].gameObject.transform.position, Quaternion.identity);
                }

                text.text = "Spawning extras";

                for (int i = 0; i < extraSpawner.Count; i++)
                {
                    float n = Random.Range(0, 100);
                    if (n <= 90) Instantiate(extras[Random.Range(0, extras.Length)], extraSpawner[i].transform.position, Quaternion.identity);
                }

                text.text = "Spawning background";

                
                    if (tier == 1) Instantiate(backGround_T1[theme_t1], transform.position, transform.rotation);
                   else if (tier == 2) Instantiate(backGround_T2[theme_t2], transform.position, transform.rotation);
                else if (tier == 3) Instantiate(backGround_T3[theme_t3], transform.position, transform.rotation);
                else if (tier >= 4) Instantiate(backGround_T4[theme_t4], transform.position, transform.rotation);
            

                if (level % 5 == 0)      //every fifth level a boss will be spawned
                {
                    text.text = "Spawning Boss";
                    bossSpawner = GameObject.FindGameObjectWithTag("bossSpawner").transform;

                    switch (tier)
                    {
                        case 1:
                            Instantiate(boss_T1[Random.Range(0, boss_T1.Length)], bossSpawner.position, Quaternion.identity);
                            break;

                        case 2:
                            Instantiate(boss_T2[Random.Range(0, boss_T2.Length)], bossSpawner.position, Quaternion.identity);
                            break;

                        case 3:
                            Instantiate(boss_T3[Random.Range(0, boss_T3.Length)], bossSpawner.position, Quaternion.identity);
                            break;

                        case 4:
                            Instantiate(boss_T4[Random.Range(0, boss_T4.Length)], bossSpawner.position, Quaternion.identity);
                            break;
                    }
                }
                loadingscreenAnimator.SetTrigger("fadeOut");

                Time.timeScale = 1f;          //back to normal

                text.text = "Endstation";


                player.transform.position = GameObject.Find("PlayerSpawn").transform.position; //setting Player position in the end
                isLoading = false;
                canChange = false;
            }          
        }


    }
}

