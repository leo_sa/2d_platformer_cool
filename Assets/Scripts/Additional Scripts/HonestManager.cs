﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoPR
{
    public class HonestManager : MonoBehaviour
    {
        GameObject honestGame;
        Animator honestAnimator;
        HonestCardManager cardManager;
        TownManager townManager;            //to increase respect when lying
        GameManager gManager;

        [SerializeField]
        TextMeshProUGUI lieCount;
        public TextMeshProUGUI nameText, respecText, socialText;            //respecText referenced in cardManager
     
        bool isPlayingHonesty;
      
        [HideInInspector]
        public int socialStatus, liesLeft;        

        private void Start()
        {
            honestGame = GameObject.Find("Honesty_Game");
            honestAnimator = honestGame.GetComponent<Animator>();
            gManager = FindObjectOfType<GameManager>();
            cardManager = FindObjectOfType<HonestCardManager>();

            Invoke("SetInactive", 0.01f);           //nullreference in honestCardManager when setinactive instantly
            lieCount.text = 7 + "x";
        }

        void SetInactive()
        {
            honestGame.SetActive(false);
        }

        public void updateStats(Dialogue dialogue)
        {
            nameText.text = "Name:" + dialogue.dialogueName;

            socialStatus = dialogue.socialStatus;
            socialText.text = "Social-level:" + socialStatus;

            respecText.text = "Rep." + StaticValues.respect;
        }


        public void enableHonesty()
        {
            Time.timeScale = 0.2f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            gManager.canSetTime = false;

            gManager.PauseOrEnablePlayer(false);
            honestGame.SetActive(true);
            isPlayingHonesty = true;           
        }

        public void disableHonesty()
        {
            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            gManager.canSetTime = true;

            gManager.PauseOrEnablePlayer(true);
            honestGame.SetActive(false);
            isPlayingHonesty = false;
        }  
        
        
        public void lie()
        {
            if(liesLeft > 0)
            {
                if (townManager != null)
                {
                    townManager.IncreaseRespect(4 * socialStatus);
                }
                else
                {
                    townManager = FindObjectOfType<TownManager>();
                    townManager.IncreaseRespect(4 * socialStatus);
                }

                cardManager.numberOfBadDeedCards++;

                cardManager.badDeedsHeldIn.Add(new int());
                cardManager.potentialDecreases.Add(new int());

                cardManager.RefreshUI();
                respecText.text = "Rep." + StaticValues.respect;
                //disableHonesty();
               
                liesLeft--;
                lieCount.text = liesLeft + "x";
            }

        }
    }

}
