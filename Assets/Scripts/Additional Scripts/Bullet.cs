﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Bullet : MonoBehaviour
    {
        Rigidbody2D rb2d;

        Gun gun;                //both for perks
        GameManager manager;

        [SerializeField]
        bool stickEnemy, stickWorld;                          //will the Bullet stick after hitting the target?
        [SerializeField]
        bool dieOnImpact = true;
        [SerializeField]
        bool destroyAfterTime = true;
        [SerializeField]
        bool collideAfterStick;

        [SerializeField]
        GameObject emptyHit, fullHit;                //effect to spawn, when no enemy is hit and when an enemy is hit.

        [SerializeField]
        float speed;

        [Range(0, 360)]
        public float accuracy;                 //how accurate is the Bullet, in Degrees?

        float originalSpeed;            //Used to calculate the LocalsScale based on Speed percentage

        public int damage = 12;

        [SerializeField]
        bool getSlowerOverTime;     //Does the Bullet get slower over time?

        [SerializeField, Range(0, 0.2f)]
        float deccelerationRate;            //The rate  of decceleration if the bool above is true

        void Start()
        {
            if (GetComponent<AudioSource>()) GetComponent<AudioSource>().pitch = Random.Range(0.95f, 1.05f);
            transform.parent = null;
            rb2d = GetComponent<Rigidbody2D>();
            originalSpeed = speed;
            if(destroyAfterTime) Destroy(gameObject, 10);
            manager = FindObjectOfType<GameManager>();

            if (FindObjectOfType<TownManager>()) FindObjectOfType<TownManager>().DecreaseRespect(1);                //decrease Respect if in town

            transform.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + Random.Range(-accuracy/2, accuracy/2));
        }

        private void Update()
        {
            if (getSlowerOverTime)                      //Decceleration, getting smaller with speed and reaching the unstoppable force of DESTROY(GAMEOBJECT) if small enough
            {
                if (speed <= 0.1f) Destroy(gameObject);
                speed = Mathf.Lerp(speed, 0, deccelerationRate);
                transform.localScale = new Vector3(speed / originalSpeed, speed / originalSpeed, speed / originalSpeed);               
            }
        }


        void FixedUpdate()
        {
            rb2d.velocity = transform.right * speed * Time.deltaTime * 80;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag != "Enemy")
            {
                Instantiate(emptyHit, transform.position, transform.rotation);           //Instantiating the Effect emptyHit
                if(stickWorld)
                {
                    rb2d.isKinematic = true;
                    speed = 0;
                    // if(collideAfterStick) gameObject.layer = 0;           //so player can collide with it to add depth. Only on "true" collision not trigger.    
                    gameObject.layer = 9;           //currently nothing collides with player after stick
                    transform.parent = collision.gameObject.transform;
                    rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
                    stickWorld = false;         //it is stuck now. It cant be changed. If this lien would be removed, it would "child" to the player.
                }
            }
            else if (collision.gameObject.tag != "Player")
            {               
                Instantiate(fullHit, transform.position, transform.rotation);           //Instantiating the Effect fullHit
                if (stickEnemy)
                {
                    rb2d.isKinematic = true;
                    speed = 0;
                    GetComponent<Collider2D>().enabled = false;
                    gameObject.layer = 9;
                    transform.parent = collision.gameObject.transform; 
                    rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
                }
                if (PerkManager.hasRecyclePerk && collision.gameObject.layer == 10) addAmmo();          // add ammo when enemy hit
            }

            if(!stickEnemy && !stickWorld) Destroy(gameObject);           

        }

        private void OnTriggerEnter2D(Collider2D collision)                 //agin, because of the collision variable i place the hit-effects two times instead of creating a method, because this would require an extra variable.
        {

            if (collision.gameObject.tag != "Enemy")            //I removed all Destroy() calls, because, if the collider is set as an trigger, it is probably supposed to slice through things
            {
                Instantiate(emptyHit, transform.position, transform.rotation);           //Instantiating the Effect emptyHit        
                if(dieOnImpact) Destroy(gameObject);

                if (stickWorld)
                {
                    rb2d.isKinematic = true;                    
                    speed = 0;
                    GetComponent<Collider2D>().enabled = false;
                    transform.parent = collision.gameObject.transform;
                    rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
                }               
            }
            else if (collision.gameObject.tag != "Player")
            {
                Instantiate(fullHit, transform.position, transform.rotation);           //Instantiating the Effect fullHit
                if (stickEnemy)
                {
                    rb2d.isKinematic = true;
                    speed = 0;
                    GetComponent<Collider2D>().enabled = false;
                    transform.parent = collision.gameObject.transform;
                    rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
                }
                if (PerkManager.hasRecyclePerk && collision.gameObject.layer == 10) addAmmo();
            }
        }

        void addAmmo()
        {
            manager.ammoAvailable++;
            manager.ammoAvailableText.text = "Left: " + manager.ammoAvailable;
        }

       
    }

}
