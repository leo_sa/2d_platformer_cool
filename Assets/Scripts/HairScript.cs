﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class HairScript : MonoBehaviour
    {
        [SerializeField]
        Transform point;
        [SerializeField]
        float radius;

        Vector3 newLocation;

        Rigidbody2D rigidBody2D;

        private void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }

        void Update()               //gameObject cant leave "radius"/Circle
        {
            rigidBody2D.velocity = Vector2.up * -0.5f;

            Vector3 distance = transform.position - point.position;
            distance = Vector3.ClampMagnitude(distance, radius);
            newLocation = point.position + distance;

            transform.position = newLocation;
        }
    }
}

