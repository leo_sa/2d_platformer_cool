﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Parralax : MonoBehaviour
    {
        [SerializeField, Range(0, 1)]
        float parralaxStrength;
        float length, startPosition;

        Transform cam;


        void Start()
        {
            cam = Camera.main.gameObject.transform;
            startPosition = transform.position.x;
            length = GetComponent<SpriteRenderer>().bounds.size.x;
        }

        void Update()
        {
            float boundsCheck = cam.position.x * (1 - parralaxStrength);
            float distance = (cam.position.x * parralaxStrength);

            transform.position = new Vector3(startPosition + distance, transform.position.y, transform.position.z);

            if (boundsCheck > startPosition + length) startPosition += length * 2;
            else if (boundsCheck < startPosition - length) startPosition -= length * 2;
        }
    }

}
