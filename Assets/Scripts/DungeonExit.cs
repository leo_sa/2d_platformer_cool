﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;          //For loading next Dungeon

namespace CoATwoPR
{
    public class DungeonExit : MonoBehaviour
    {       
        [SerializeField]
        LoadSceneMode loadscene;

        GameManager gameManager;

        Animator loadingscreenAnimator;             //reference to the loadingscreen, to trigger the fade In
        Animator animator;

        [SerializeField]
        AudioClip open, close;
        AudioSource audioSource;

        [SerializeField]
        string customSceneName;

        [SerializeField]
        bool canMove, loadTutorial = false, loadDifferentScene;

        bool interact;
        bool interActOnce = true;
        bool canCheckInput;


        void Start()
        {
            if(GetComponent<Animator>()) animator = GetComponent<Animator>();
            if (GetComponent<AudioSource>()) audioSource = GetComponent<AudioSource>();
            loadingscreenAnimator = GameObject.Find("LoadingScreen").GetComponent<Animator>();
            gameManager = FindObjectOfType<GameManager>();
        }


        void Update()
        {
            if (interActOnce)
            {
                if (interact)
                {
                    loadingscreenAnimator.SetTrigger("fadeIn");
                    if (!loadDifferentScene)
                    {
                        if (!loadTutorial) Invoke("ExitDungeon", 0.2f);
                        else LoadTutorialMethod();
                    }
                    else
                    {
                        SceneManager.LoadScene(customSceneName, loadscene);
                    }
                    interActOnce = false;
                }
            }
          

            if (canCheckInput)
            {
                if (Input.GetKeyDown("e")) interact = true;
                else interact = false;
            }

            if (transform.position.y <= -200) ExitDungeon();           //when this conditions of the transform position is met, the Object probably glitched through a wall
        }

        void LoadTutorialMethod()
        {          
            SceneManager.LoadScene("TutorialRoom", loadscene);
        }

        public void exitDungeonButton()
        {
            interActOnce = true;
            interact = true;
        }

        void ExitDungeon()           //public for button usage.
        {
            gameManager.level++;
            FindObjectOfType<DialogueManager>().EndDialogue();
            if ((gameManager.level % 6 == 0))                                                      //Every 6 Level the Player will visit a Town, thus music has to stop
            {
                GameObject.Find("Music").GetComponent<AudioSource>().Stop();
                SceneManager.LoadScene("town", loadscene);
                gameManager.tier++;
            }
            else
            {
                if (!GameObject.Find("Music").GetComponent<AudioSource>().isPlaying) GameObject.Find("Music").GetComponent<AudioSource>().Play();       //if not playing for some reason, play please!
                SceneManager.LoadScene("Dungeon", loadscene);
            }
            
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {   
                if(animator != null) animator.SetTrigger("activate");           //two prefabs have an isntance of the dungeon. One is animated, one not.
                if(audioSource != null)
                {
                    audioSource.clip = open;
                    audioSource.Stop();
                    audioSource.pitch = Random.Range(0.95f, 1.05f);
                    audioSource.Play();
                }
                canCheckInput = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                if (animator != null) animator.SetTrigger("deactivate");
                if (audioSource != null)
                {
                    audioSource.clip = close;
                    audioSource.Stop();
                    audioSource.pitch = Random.Range(0.95f, 1.05f);
                    audioSource.Play();
                }
                canCheckInput = false;
            }
        }

    }
}

