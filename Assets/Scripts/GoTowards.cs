﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CoATwoPR
{
    public class GoTowards : MonoBehaviour
    {

        [SerializeField]
        Transform point;

        Rigidbody2D rigidBody2D;

        [SerializeField]
        float speed = 1;

        void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }


        void Update()
        {
            rigidBody2D.velocity = (point.position - transform.position) * speed;
        }
    }

}
