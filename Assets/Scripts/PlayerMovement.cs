﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;           //for dashSlider

namespace CoATwoPR
{
    public class PlayerMovement : MonoBehaviour2            //disclaimer: Some old code-pieces are not deleted, because i sometimes like to re-use certain bits of them.
    {
        [HideInInspector]
        public Rigidbody2D rb2d;
        [HideInInspector]
        public Gun gun;         //for socialmode

        [HideInInspector]
        public SpriteRenderer spriteRenderer;           //referenced in meeleWeapon for recoil
        Animator anim;

        Camera cam;             // to zoom in when in social mode.
        CameraFollowing shake;      //for screenShake

        [SerializeField]
        FlipSpriteXY spriteFlipArms, spriteFlipHair;
        FlipSpriteXY spriteFlip;

        [SerializeField]
        LayerMask lay;

        Transform bullPo;           //for additional dash, bullPo always faces MousePosition
        Transform climbCast, climbCastB;

        [SerializeField]
        GameObject jumpEffect, landEffect;
        [SerializeField]
        GameObject slice;

        [SerializeField]
        AudioClip jumpClip, dashClip, slideClip, fallClip, wallRun;
        
        AudioSource audioSource;

        Slider dashSlider;

        Vector3 originalTransform;          //used for climbing


        [SerializeField]
        int deccelerationFrames = 3;      //used To set the Amount of Frames tro decelerate
        [SerializeField]
        int maxJumps = 2;                               //Max amounts of Player-Jumps

        int jumpAmounts;        //Amounts the Player can Jump.
        int lookDir;            //Looking direction, either 1 or -1            
        int wallDir;        //Direction to wallJump to
        int originalWallDir;        //used for climbing

        [HideInInspector]
        public float grapForceX, grapForceY;               //the Extraforce from the grappling hook
        [HideInInspector]
        public float extraForce, extraForceY;       //Extra Force applied to the rigidbody. Includes Things such as Recoil from a Gun or Sliding.

        [SerializeField]
        float jumpPower = 3;            // Height of the Jump
        [SerializeField]
        float slideForce = 2;
        [SerializeField, Range(0, 1)]
        float extraForceDeccelerationSpeed = 0.5f, grapForceDeccelerationSpeed = 0.04f;
        [SerializeField]
        float movementSpeed = 10;                        // used to set the amount of movementspeed
        [SerializeField]
        float accelerationTime = 0.1f;                 //used To set the Amount of Time tro accelerate

        float gravity;      //Used for a gravity reference, for later usage       
        float slideTimer;   //used to calculate the activity od the bool above. Meaning, when it will be set back to inactive.
        float speed;                        //Speed, used internally
        float originalCamSize;
        float accelerationTimer;                     //used for Acceleration
        float direction;                  //Direction of movement
        float maxShiftDuration = 0.2f, maxShiftDashCooldown = 0.6f;       //shift dash duration, cooldown
        float shiftDashTimer;           //timer for Shift Dash  
        float dir;          // the direction. Two variables fot he direciton have to best to correctly decellerate.
        float wallrunSpeed;         //reference to add rb2d.velocity.x for momentum
        float sliceTimer;
        float continiousJumpTimer = 0.2f;
        float additionalJumpHeight = 1, additionalSpeed = 1;


        [HideInInspector]
        public bool canMove = true;
        [HideInInspector]
        public bool walled;                //if Walljump has to be activated/ If Colliding with wall to the left or right. Public for GUN  

        bool canClimb;
        bool socialMode;
        bool dashed;            //for the Coroutine Dash
        bool wallJumped;            //if Walljump, velocity has to be set differently
        bool grounded;      // is the Player currently vertically grounded? (used to differenciate between slide and Dash);
        bool isSliding;     //Used to determine slide. Used to calculate if enemy Collison can be ignored/dodged.       
        bool canPlayFallSound;      //to not constantly play the "grounded sound" a bool check has to follow.
        bool canShiftDashAgain;         //Bool , so player cant just hold shift to constantly dash
        bool wallRunning;
        bool canSetCam;
        bool canSliceAgain = true;
        bool applyLastDashForce;
        bool canJumpDash;
        bool roofed;
        bool shiftIsPressed;
        bool isClimbing;
        bool isJumping;
        bool flipSpriteOnce = true;        //used to not update spriteRenderers every single frame


        void Start()
        {
            rb2d = GetComponent<Rigidbody2D>();           
            spriteRenderer = GetComponent<SpriteRenderer>();          
            anim = GetComponent<Animator>();
            climbCast = transform.GetChild(1).transform;
            climbCastB = transform.GetChild(1).transform.GetChild(0).transform;
            cam = Camera.main;           
            gun = FindObjectOfType<Gun>();
            shake = FindObjectOfType<CameraFollowing>();
            //dashSlider = GameObject.Find("ShiftDashSlider").GetComponent<Slider>();       //the slider is currently unused but may be reused at a later state
            audioSource = GetComponent<AudioSource>();
            bullPo = GameObject.Find("bulPo").transform;
            spriteFlip = GetComponent<FlipSpriteXY>();

            SetStartValues();
        }

        void SetStartValues()
        {
            //dashSlider.maxValue = maxShiftDuration;
            //dashSlider.value = maxShiftDuration;
            jumpAmounts = maxJumps;
            shiftDashTimer = maxShiftDuration;
            originalCamSize = cam.orthographicSize;
            gravity = rb2d.gravityScale;
        }


        void Update()
        {
            if (walled && !flipSpriteOnce)             //Player needs to always face the wall while wallrunning, when not the sprites look worse
            {
                spriteFlip.canFlip = false;
                spriteFlipArms.canFlip = false;

                if (wallDir == 1)
                {
                    spriteRenderer.flipX = true;
                    spriteFlipArms.spriteRenderer.flipX = true;
                }
                else
                {
                    spriteRenderer.flipX = false;
                    spriteFlipArms.spriteRenderer.flipX = false;
                }

                flipSpriteOnce = true;
            }
            else if(flipSpriteOnce && !walled)
            {               
                spriteFlip.canFlip = true;
                spriteFlipArms.canFlip = true;

                flipSpriteOnce = false;
            }

            shiftIsPressed = Input.GetKey(KeyCode.LeftShift);

            anim.SetBool("grounded", grounded);         //Always, so the player cant get stuck in jumping anim
            anim.SetBool("isWalled", walled);        

            if (!grounded) canPlayFallSound = true;         //when in air, the sound can play again

            if (canMove)
            {
                if (!socialMode)
                {
                    if (!wallRunning)
                    {
                        if (canSetCam)
                        {
                            //if (Mathf.Abs((cam.orthographicSize - originalCamSize)) > 0.1f) cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalCamSize, 0.1f);
                           // else if (cam.orthographicSize != originalCamSize) 
                            //{
                               // cam.orthographicSize = originalCamSize;
                               // canSetCam = false;
                            //}

                                                     //currently Cut out because of bugs related to gunsafety
                        }
                        
                    }
                  
                    if (spriteRenderer.flipX) lookDir = -1;
                    else lookDir = 1;

                    SpeedAcceleration();
                    NewJump();
                    SlideCheck();
                    GravityModifier();              
                    CheckWall();
                  
                }
                else
                {
                    canSetCam = true;
                    //if(Mathf.Abs(cam.orthographicSize - (originalCamSize / 3)) > 0.1f) cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalCamSize / 3, 0.2f);
                    //else if (cam.orthographicSize != originalCamSize / 3) cam.orthographicSize = originalCamSize / 3;             
                            //related to the currently cut part above
                    GravityModifier();      //has to be in, so gravity cant be stuck in a larger value then intended
                    SpeedAcceleration();
                }

                if (Input.GetKeyDown("b") && !gun.isReloading && !gun.isSHooting)              //if canNotMove, probably paused or trading, so an interaction with the socialMode should not be possible.
                {
                    gun.gunSafety();
                    if (socialMode) socialMode = false;
                    else socialMode = true;
                }
                DecreaseForces();

            }

           
        }

        private void FixedUpdate()              //using FixedUpdate, because fixedUpdate is in Sync with the Unity-Physics system.
        {
            CheckWall();

            if (canClimb) Climb();           

            if (!isClimbing)
            {             

                if (canMove)
                {
                    if (!socialMode)
                    {
                        if (!shiftIsPressed)
                        {
                            if (!walled)
                            {
                                rb2d.velocity = new Vector2((direction * speed + (extraForce)) * additionalSpeed * 100 * Time.deltaTime + grapForceX, grapForceY + rb2d.velocity.y * Time.deltaTime * 50 + extraForceY);
                            }
                            else if (direction != dir)
                            {
                                rb2d.velocity = new Vector2(direction * (speed / 1.4f) + extraForce * additionalSpeed * 100 * Time.deltaTime + grapForceX, grapForceY + -wallDir * Input.GetAxisRaw("Horizontal") * (Mathf.Abs(speed) / 1.4f) * Time.deltaTime * 100 + extraForceY);
                            }
                        }
                        else rb2d.velocity = new Vector2(direction * 100 * Time.deltaTime, rb2d.velocity.y * Time.deltaTime * 50);


                    }
                    else
                    {
                        rb2d.velocity = new Vector2(direction * 100 * Time.deltaTime, rb2d.velocity.y * Time.deltaTime * 50);
                    }                   
                }
            }
            else
            {
                ClimbMove();
            }
            
        }

        void DecreaseForces()           //extraforces are additional forces used by weapons or sliding. grapForce is used by the grappling hook.
        {
            if (extraForce != 0) extraForce = Mathf.Lerp(extraForce, 0, extraForceDeccelerationSpeed);
            if (extraForceY != 0) extraForceY = Mathf.Lerp(extraForceY, 0, extraForceDeccelerationSpeed * 9);

            if (grapForceX != 0) grapForceX = Mathf.Lerp(grapForceX, 0, grapForceDeccelerationSpeed / 2);
            if (grapForceY != 0) grapForceY = Mathf.Lerp(grapForceY, 0, grapForceDeccelerationSpeed * 2);


            if (additionalJumpHeight != 1) additionalJumpHeight = Mathf.Lerp(additionalJumpHeight, 1, 0.01f);
            if (additionalSpeed != 1) additionalSpeed = Mathf.Lerp(additionalSpeed, 1, 0.01f);
        }

        void CheckAnimator()
        {
            int dir;
            anim.SetBool("isRunning", true);
            if (spriteRenderer.flipX)
            {
                dir = (int)direction * -1;
            }
            else dir = (int)direction;
            anim.SetFloat("Direction", dir);
        }

        void SpeedAcceleration()
        {           
            dir = Input.GetAxisRaw("Horizontal");

            if (dir != 0)
            {
                if(grounded) direction = Input.GetAxis("Horizontal");            //Setting the Direction. Sensitivity was changed to 8 instead of 3 in the project settings
                else direction = Mathf.Lerp(direction, Input.GetAxisRaw("Horizontal"), 0.1f);
            }
            else
            {
                if (Mathf.Abs(direction) >= 0.05f) direction = Mathf.Lerp(direction, 0, 0.2f);              //Through lerp "0" is reached after a long time, so it has to be set to 0 manually
                else direction = 0;
            }

            if (direction != 0)
            {
                CheckAnimator();
                if (accelerationTimer <= accelerationTime)
                {
                    speed = Mathf.Lerp(speed, movementSpeed, 0.3f);             //Main Acceleration
                    accelerationTimer += Time.deltaTime;
                }
            }
            else
            {
                anim.SetBool("isRunning", false);
                speed = 0;
                accelerationTimer = 0;
            }

        }           //Acceleration of the Playerspeed    
       
        void NewJump()              //Current Jump Method
        {   
            
            if (!walled)
            {
              
                if (grounded)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        MyPlay(audioSource, jumpClip);          //playing jump sound my way
                        Instantiate(jumpEffect, transform.position, Quaternion.identity);
                        isJumping = true;

                        rb2d.gravityScale = gravity;
                        rb2d.velocity += Vector2.up * jumpPower * 5 * additionalJumpHeight;
                    }
                    if (Input.GetKeyUp(KeyCode.Space)) jumpAmounts--;
                }
                else             //JumpDash
                {
                    if (Input.GetKeyDown(KeyCode.Space) && jumpAmounts >= 0)
                    {
                        float dirX = Input.GetAxisRaw("Horizontal");
                        float dirY = Input.GetAxisRaw("Vertical");

                        if (dirY != -1) dirY = 1.3f;

                        rb2d.gravityScale = gravity;

                        extraForce = dirX * 7;
                        extraForceY = dirY * 14;

                        jumpAmounts--;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                        extraForce = wallDir * 10;
                        extraForceY = 15;              
                }
            }

            if (isJumping && Input.GetKey(KeyCode.Space) && continiousJumpTimer >= 0)           //contunuousJump
            {
                rb2d.velocity += Vector2.up * jumpPower;
                continiousJumpTimer -= Time.deltaTime;
            }
            else if (isJumping && Input.GetKeyUp(KeyCode.Space))
            {
                continiousJumpTimer = 0.2f;
                isJumping = false;
            }

        }

        void Climb()
        {
            if (!isClimbing)
            {
                if(wallDir == 1)
                {
                    if (Input.GetKey(KeyCode.Space) || Input.GetKey("a")) StartCoroutine(DoClimb());
                }
                else if(wallDir == -1)
                {
                    if (Input.GetKey(KeyCode.Space) || Input.GetKey("d")) StartCoroutine(DoClimb());
                }
            }
        }

        void ClimbMove()
        {
            rb2d.velocity = new Vector3(0, 0, 0);       //stopping movement
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(originalTransform.x - originalWallDir / 1.4f, originalTransform.y + 0.7f, 0), 0.2f);
        }

        void GravityModifier()
        {
            if (!walled)
            {
                if (!grounded) rb2d.gravityScale += Time.deltaTime * 3;
                else rb2d.gravityScale = gravity;

                if (Input.GetKeyDown("s")) rb2d.gravityScale *= 6;
                else if (Input.GetKeyUp("s")) rb2d.gravityScale /= 6;              
            }
            else rb2d.gravityScale = gravity / 2;


        }

        void SlideCheck()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl) && slideTimer == 0)
            {
                if (grounded) Slide();
                else if(!dashed) StartCoroutine(Dash());
            }

            if (slideTimer > 0) slideTimer -= Time.deltaTime;
            else if(slideTimer < 0)
            {
                Physics2D.IgnoreLayerCollision(9, 10, false);
                isSliding = false;
                slideTimer = 0;
            }

            if (isSliding) Physics2D.IgnoreLayerCollision(9, 10, true);                 //Dodge Enemy while sliding for exactly 0.5f Seconds
        }

        void Slide()
        {
            MyPlay(audioSource, slideClip);
            extraForce = slideForce * direction;
            isSliding = true;
            slideTimer = 1f;
            anim.SetTrigger("slide");
        }

        void CheckWall()                                    //Including GroundCheck
        {
            //left Check
            if(Physics2D.OverlapBox(new Vector2(transform.position.x - 0.1f, transform.position.y), new Vector2(0.1f, 0.5f),0, lay))          //Checking WallPosition through PhysicsOverlap
            {
                wallDir = 1;
                walled = true;
                direction = Mathf.Clamp(direction, 0, 1);          //Clamping direction, so the Player cannot press him/herself against the wall which disables a fluent wallJump
            }
           // else if (Physics2D.Raycast(transform.position, transform.right, 0.2f, lay))
            else if (Physics2D.OverlapBox(new Vector2(transform.position.x + 0.1f, transform.position.y), new Vector2(0.1f, 0.5f), 0, lay))
            {
                wallDir = -1;
                walled = true;
                direction = Mathf.Clamp(direction, -1, 0);
            }
            else
            {
                wallDir = 0;
                walled = false;
            }

            if(wallDir != 0)        //if walled
            {
                //dashSlider.value = maxShiftDuration;                    //resetting dash
                jumpAmounts = maxJumps;
                shiftDashTimer = maxShiftDuration;

                if (wallDir == dir)             //"repel" from wall
                {
                    extraForce += wallDir * 3;
                    extraForceY += 2;
                }
            }

            if (Physics2D.Raycast(transform.position, transform.up, -0.5f, lay)) grounded = true;               //groundcheck
            else grounded = false;

            if (Physics2D.Raycast(transform.position, transform.up, 0.5f, lay)) roofed = true;               //roofCheck
            else roofed = false;

            if (spriteRenderer.flipX)
            {
                if (!Physics2D.Raycast(climbCast.position, transform.right, -0.3f, lay) && Physics2D.Raycast(climbCastB.position, transform.right, -0.05f))
                {
                    if (walled) canClimb = true;
                    else canClimb = false;
                }
                else canClimb = false;
            }
            else
            {
                if (!Physics2D.Raycast(climbCast.position, transform.right, 0.3f, lay) && Physics2D.Raycast(climbCastB.position, transform.right, 0.05f))       
                {
                    if (walled) canClimb = true;
                    else canClimb = false;
                }
                else canClimb = false;
            }          

        }

     

        public void BuffStats()
        {
            int chance;
            chance = Random.Range(1, 3);

            switch (chance)
            {
                case 1:
                    additionalJumpHeight += .5f;
                    break;
                case 2:
                    additionalSpeed += 0.5f;
                    break;
                case 3:
                    additionalJumpHeight += .25f;
                    additionalSpeed += .25f;
                    break;
            }
        }

        IEnumerator WallJump()
        {
            wallJumped = true;
            // extraForce += (Mathf.Abs(rb2d.velocity.x + speed) * wallDir) * 0.5f;
            // rb2d.velocity += Vector2.up;

            rb2d.velocity = new Vector2(Mathf.Abs(rb2d.velocity.x + speed) * wallDir * 2, jumpPower * 0.75f);

            yield return new WaitForSeconds(0.2f);

            wallJumped = false;
        }

        IEnumerator Dash()
        {
            if (!dashed)
            {
                isSliding = true;           //to also dodge enemies
                extraForce += 12 * direction;              
                dashed = true;
                slideTimer = 0.001f;
            }

            yield return new WaitForSeconds(0.08f);

          
            isSliding = false;
            dashed = false;
        }

        IEnumerator DoClimb()        
        {
            anim.SetTrigger("climbWall");

            isClimbing = true;
            originalTransform = transform.localPosition;
            originalWallDir = wallDir;

            yield return new WaitForSeconds(0.1f);

            isClimbing = false;
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (canPlayFallSound && grounded)
            {
                Instantiate(landEffect, collision.GetContact(0).point, Quaternion.identity);               //also only once at the collision point
                MyPlay(audioSource, fallClip);
                canPlayFallSound = false;
            }

            //dashSlider.value = maxShiftDuration;                    //resetting dash
            shiftDashTimer = maxShiftDuration;

            if(!roofed) jumpAmounts = maxJumps;         //resetting the Jumps;  

            if (collision.gameObject.tag == "Jumper")
            {
                rb2d.AddForce(Vector2.up * 13, ForceMode2D.Impulse);
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if(isSliding && !grounded) extraForce += slideForce * direction;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "WallRun")
            {
                canSetCam = true;
                MyPlay(audioSource, wallRun);
                wallrunSpeed = rb2d.velocity.x;

                continiousJumpTimer = 0.1f;
                isJumping = false;
                jumpAmounts = maxJumps; //resetting jump

                wallRunning = true;
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (wallRunning)
            {
                if (Input.GetKeyDown(KeyCode.Space)) wallRunning = false;               //no wallrun when trying to jump
             
                extraForce = wallrunSpeed;
                rb2d.velocity = new Vector2(rb2d.velocity.x, -0.1f);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "WallRun")
            {
                wallRunning = false;
                rb2d.gravityScale = gravity;
            }
           
        }

    }
}

