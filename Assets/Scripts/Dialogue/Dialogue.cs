﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class Dialogue : MonoBehaviour
    {


        [TextArea(3, 10)]
        public string[] sentenc;

        public string dialogueName;

        public Sprite portrait;

        public bool canPlayHonesty;

        [Range(1, 3)]
        public int socialStatus = 1;


    }

}
