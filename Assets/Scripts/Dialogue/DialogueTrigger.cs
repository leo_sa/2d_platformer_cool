﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class DialogueTrigger : Dialogue
    {

        public Dialogue dia;

        bool canInteract;

        AudioSource audioSource;

        GameObject e_interface;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
            if (transform.childCount > 1)
            {
                e_interface = transform.GetChild(1).gameObject;
                e_interface.SetActive(false);
            }
        }


        private void Update()
        {
            if (canInteract)
            {
                if (Input.GetKeyDown("e")) TriDia();
            }
        }
        public void TriDia()
        {
            if(e_interface != null) e_interface.SetActive(false);
            FindObjectOfType<DialogueManager>().StartDialogue(dia, audioSource, transform);
        }      

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Player")
            {
                if (e_interface != null) e_interface.SetActive(true);
                canInteract = true;
            }
        }
     
        private void OnTriggerExit2D(Collider2D collision)
        {

            if (collision.gameObject.name == "Player")
            {
                if (e_interface != null) e_interface.SetActive(false);
                canInteract = false;
            }
        }
    }
}

