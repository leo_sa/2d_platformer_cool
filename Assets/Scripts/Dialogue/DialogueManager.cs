﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;           //used for the Portrait
using TMPro;

namespace CoATwoPR
{
    public class DialogueManager : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI nameText;

        [SerializeField]
        TextMeshProUGUI senText;

        Queue<string> sentences;

        Animator animator;      //for smooth transitions

        bool currentlySpeaking;         //check if dialogue is currenty """"active"""", so trigger wont be set twice


        AudioSource audioSource;

        Transform player, npc;

        Image portraitImage;

        GameObject honetButton;

        HonestManager honestManager;


        void Start()
        {
            sentences = new Queue<string>();
            animator = GetComponent<Animator>();
            player = GameObject.Find("Player").transform;
            portraitImage = transform.GetChild(3).GetComponent<Image>();
            honetButton = transform.GetChild(4).gameObject;
            honestManager = FindObjectOfType<HonestManager>();


        }

        private void Update()
        {
            if (currentlySpeaking)
            {
                if (Vector2.Distance(player.position, npc.position) > 4) EndDialogue();
            }
        }

        public void StartDialogue(Dialogue dia, AudioSource aus, Transform transform)
        {
            honetButton.SetActive(dia.canPlayHonesty);
            if (dia.canPlayHonesty) honestManager.updateStats(dia);
            
            
            npc = transform;
            audioSource = aus;
            portraitImage.sprite = dia.portrait;
          

            if (!currentlySpeaking)
            {
                animator.SetTrigger("fadeIn");
                animator.ResetTrigger("fadeOut");
            }
            currentlySpeaking = true;
            nameText.text = dia.dialogueName;
            sentences.Clear();          

            foreach (string sentence in dia.sentenc)
            {
                sentences.Enqueue(sentence);
            }

            DisNex();
        }

        void PlayNoVoice(AudioSource audioSource)
        {
            //audioSource.Stop();
            audioSource.pitch = Random.Range(0.99f, 1.01f);
            audioSource.PlayDelayed(0.01f);

        }

        public void DisNex()
        {
            if (sentences.Count == 0)
            {
                EndDialogue();
                return;
            }


            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
            //senText.text = sentence;

        }


        IEnumerator TypeSentence(string sentence)
        {
            int counter;
            counter = 0;
            senText.text = "";
            foreach (char letter in sentence.ToCharArray())
            {
                counter++;
                senText.text += letter;
                if(((counter % 6) == 0) || counter == 1) PlayNoVoice(audioSource);                             //every thrid and first character voice will play
                yield return null;
            }
        }

        public void EndDialogue()
        {
            currentlySpeaking = false;
            animator.SetTrigger("fadeOut");
            animator.ResetTrigger("fadeIn");
            senText.text = "";
        }
    }
}

